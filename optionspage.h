#ifndef OPTIONSPAGE_H
#define OPTIONSPAGE_H

#include <QObject>
#include "coreplugin/dialogs/ioptionspage.h"

#include "settings.h"
#include "optionswidget.h"

namespace QtUnitTest
{
namespace Internal
{

class OptionsPage : public Core::IOptionsPage
{
    Q_OBJECT
public:
    explicit OptionsPage(const Settings settings, QObject *parent = 0);
    ~OptionsPage();
    QWidget *widget();
    void apply();
    void finish();

    void setSettings(const Settings settings) {_settings = settings;}
signals:
    void settingsChanged(Settings settings);

private:
    Settings _settings;
    QPointer<OptionsWidget> _optionsWidget;
};
}
}

#endif  // OPTIONSPAGE_H
