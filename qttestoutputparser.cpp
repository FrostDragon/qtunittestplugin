#include "qttestoutputparser.h"

#include "constants.h"
#include "teststorage.h"

#include <QStringList>
#include <QRegularExpression>
#include <QDir>
#include <QFileInfo>

using namespace QtUnitTest::Internal;
using namespace QtUnitTest::Constants;

const QRegularExpression TEST_STARTING_TEXT(QLatin1String("^Starting *.+\\.\\.\\.$"),
                                            QRegularExpression::MultilineOption
                                                | QRegularExpression::DotMatchesEverythingOption);

const QRegularExpression
    TEST_EXITED_WITH_TEXT(QLatin1String("^ *.+ exited with code \\d+$"),
                          QRegularExpression::MultilineOption
                              | QRegularExpression::DotMatchesEverythingOption);

QtTestOutputParser::QtTestOutputParser(QObject *parent) : QObject(parent) {}

void QtTestOutputParser::init(const QString workingDirectory,
                              const QStringList actualTestSourceFiles)
{
    _workingDirectory = workingDirectory;
    _actualTestSourceFiles = actualTestSourceFiles;
    _xmlStreamReader.clear();
    _isXmlReaderReset = true;
    _lastTestSuite.clear();
    _lastTestMethod.clear();
    resetTestRelatedVariables();
}

bool QtTestOutputParser::parse(const QString &message)
{
    QString localMessage = message;
    if (localMessage.contains(QStringLiteral("Cannot retrieve debugging output.")))
        localMessage.remove(QStringLiteral("Cannot retrieve debugging output."));

    if (_isXmlReaderReset && localMessage.contains(TEST_STARTING_TEXT))
        localMessage.replace(TEST_STARTING_TEXT, QLatin1String(""));

    //    if (localMessage.contains(TEST_EXITED_WITH_TEXT))
    //    {
    //        qDebug() << "one more test";
    //    }
    //        localMessage.replace(TEST_EXITED_WITH_TEXT, QLatin1String(""));

    localMessage = localMessage.trimmed();
    if (localMessage.isEmpty())
        return true;
    _xmlStreamReader.addData(localMessage);
    _isXmlReaderReset = false;
    while (!_xmlStreamReader.atEnd())
    {
        _xmlStreamReader.readNext();
        if (!_xmlStreamReader.hasError() && _xmlStreamReader.isStartElement())
        {
            QString tagName = _xmlStreamReader.name().toString();
            if (tagName == QStringLiteral("Environment"))
                _xmlStreamReader.skipCurrentElement();
            else if (tagName == QStringLiteral("TestCase"))
            {
                resetTestRelatedVariables();
                processTestSuiteTag();
            }
            else if (tagName == QStringLiteral("TestFunction"))
            {
                resetTestRelatedVariables();
                processTestMethodTag();
            }
            else if (tagName == QStringLiteral("Incident"))
                processIncidentTag();
            else if (tagName == QStringLiteral("Message"))
                processMessageTag();
            else if (tagName == QStringLiteral("DataTag"))
                processDataTag();
            else if (tagName == QStringLiteral("Description"))
                processDescriptionTag();
            else if (tagName == QStringLiteral("Duration"))
                processDurationTag();
        }
        else
        {
            if (isPrematureEndOfDocument())
            {
//                qDebug() << "new xml chunk is incomplete. TestCase or TestFunction tag not "
//                            "read";
                return true;
            }
//            if (_xmlStreamReader.hasError())
//                qDebug() << "failed to read next start element" << _xmlStreamReader.errorString();
        }
    }
    return true;
}

QString QtTestOutputParser::workingDirectory() const { return _workingDirectory; }

void QtTestOutputParser::setWorkingDirectory(const QString &workingDirectory)
{
    _workingDirectory = workingDirectory;
}

void QtTestOutputParser::processTestSuiteTag()
{
    QString newTestSuite = _xmlStreamReader.attributes().value(QStringLiteral("name")).toString();
    if (newTestSuite.isEmpty())
    {
        qDebug() << "corrupted TestCase tag attributes";
        return;
    }
    _lastTestSuite = newTestSuite;
}

void QtTestOutputParser::processTestMethodTag()
{
    QString newTestMethod = _xmlStreamReader.attributes().value(QStringLiteral("name")).toString();
    if (newTestMethod.isEmpty())
    {
        qDebug() << "corrupted TestFunction tag attributes";
        return;
    }
    _lastTestMethod = newTestMethod;
}

bool QtTestOutputParser::processIncidentTag()
{
    if (_xmlStreamReader.name() != QStringLiteral("Incident"))
        return true;
    _lastFile = toAbsoluteFilename(
        _xmlStreamReader.attributes().value(QStringLiteral("file")).toString());
    _lastLine = _xmlStreamReader.attributes().value(QStringLiteral("line")).toInt();
    _lastState = testState(_xmlStreamReader.attributes().value(QStringLiteral("type")).toString());
    TestStorage *storage = TestStorage::instance();
    QList< TestInfo > compatibleTestInfo
        = storage->findTestInfoWithFiles(QT_TEST_FRAMEWORK, _lastTestSuite, _lastTestMethod,
                                         _actualTestSourceFiles);
    foreach (TestInfo testInfo, compatibleTestInfo)
    {
        ExecutionInfo execInfo = storage->executionInfoFor(testInfo);
        Message newMessage(_lastState, _lastFile, _lastLine);
        execInfo.appendMessage(newMessage);
        storage->setExecutionInfo(testInfo, execInfo);
    }
    return true;
}

void QtTestOutputParser::processDurationTag()
{
    if (_xmlStreamReader.name() == QStringLiteral("Duration"))
    {
        double duration = _xmlStreamReader.attributes().value(QStringLiteral("msecs")).toDouble();
        TestStorage *storage = TestStorage::instance();
        QList< TestInfo > compatibleTestInfo
            = storage->findTestInfoWithFiles(QT_TEST_FRAMEWORK, _lastTestSuite, _lastTestMethod,
                                             _actualTestSourceFiles);
        foreach (TestInfo testInfo, compatibleTestInfo)
        {
            ExecutionInfo execInfo = storage->executionInfoFor(testInfo);
            execInfo.setDurationInMiliseconds(duration);
            storage->setExecutionInfo(testInfo, execInfo);
        }
    }
}

void QtTestOutputParser::processDataTag()
{
    if (_xmlStreamReader.name() == QStringLiteral("DataTag"))
    {
        _lastDataTagText = _xmlStreamReader.readElementText();
        TestStorage *storage = TestStorage::instance();
        QList< TestInfo > compatibleTestInfo
            = storage->findTestInfoWithFiles(QT_TEST_FRAMEWORK, _lastTestSuite, _lastTestMethod,
                                             _actualTestSourceFiles);
        foreach (TestInfo testInfo, compatibleTestInfo)
        {
            ExecutionInfo execInfo = storage->executionInfoFor(testInfo);
            execInfo.addDataTextToLastMessage(_lastDataTagText);
            storage->setExecutionInfo(testInfo, execInfo);
        }
    }
}

void QtTestOutputParser::processDescriptionTag()
{
    if (_xmlStreamReader.name() == QStringLiteral("Description"))
    {
        _lastDescriptionText = _xmlStreamReader.readElementText();
        TestStorage *storage = TestStorage::instance();
        QList< TestInfo > compatibleTestInfo
            = storage->findTestInfoWithFiles(QT_TEST_FRAMEWORK, _lastTestSuite, _lastTestMethod,
                                             _actualTestSourceFiles);
        foreach (TestInfo testInfo, compatibleTestInfo)
        {
            ExecutionInfo execInfo = storage->executionInfoFor(testInfo);
            execInfo.addTextToLastMessage(_lastDescriptionText);
            storage->setExecutionInfo(testInfo, execInfo);
        }
    }
}

bool QtTestOutputParser::processMessageTag()
{
    if (_xmlStreamReader.name() != QStringLiteral("Message"))
        return true;
    _lastFile = toAbsoluteFilename(
        _xmlStreamReader.attributes().value(QStringLiteral("file")).toString());
    _lastLine = _xmlStreamReader.attributes().value(QStringLiteral("line")).toInt();
    _lastState = testState(_xmlStreamReader.attributes().value(QStringLiteral("type")).toString());
    TestStorage *storage = TestStorage::instance();
    QList< TestInfo > compatibleTestInfo
        = storage->findTestInfoWithFiles(QT_TEST_FRAMEWORK, _lastTestSuite, _lastTestMethod,
                                         _actualTestSourceFiles);
    foreach (TestInfo testInfo, compatibleTestInfo)
    {
        ExecutionInfo execInfo = storage->executionInfoFor(testInfo);
        Message newMessage(_lastState, _lastFile, _lastLine);
        execInfo.appendMessage(newMessage);
        storage->setExecutionInfo(testInfo, execInfo);
    }
    return true;
}

void QtTestOutputParser::resetTestRelatedVariables()
{
    _lastFile.clear();
    _lastLine = 0;
    _lastState = Message::TS_PASS;
    _lastDataTagText.clear();
    _lastDescriptionText.clear();
}

bool QtTestOutputParser::isPrematureEndOfDocument() const
{
    return _xmlStreamReader.hasError()
           && _xmlStreamReader.error() == QXmlStreamReader::PrematureEndOfDocumentError;
}

QString QtTestOutputParser::toAbsoluteFilename(const QString &relativeFilename) const
{
    QDir dir(_workingDirectory);
    QFileInfo fileInfo(dir, relativeFilename);
    return fileInfo.canonicalFilePath();
}

Message::TestState QtTestOutputParser::testState(const QString state) const
{
    if (state == QStringLiteral("pass"))
        return Message::TS_PASS;
    else if (state == QStringLiteral("fail"))
        return Message::TS_FAIL;
    else if (state == QStringLiteral("skip"))
        return Message::TS_NOT_RUN;
    else if (state == QStringLiteral("warn"))
        return Message::TS_WARN;
    qDebug() << __FUNCTION__ << "unknown test state";
    return Message::TS_UNDEFINED;
}
QStringList QtTestOutputParser::actualTestSourceFiles() const { return _actualTestSourceFiles; }

void QtTestOutputParser::setActualTestSourceFiles(const QStringList &actualTestSourceFiles)
{
    _actualTestSourceFiles = actualTestSourceFiles;
}
