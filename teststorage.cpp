#include "teststorage.h"

#include <QDebug>
#include <QFileInfo>

using namespace QtUnitTest::Internal;

QString TestInfo::absoluteFilename() const { return _absoluteFilename; }

void TestInfo::setAbsoluteFilename(const QString &absoluteFilename)
{
    _absoluteFilename = absoluteFilename;
}

QString TestInfo::testFramework() const { return _testFramework; }

void TestInfo::setTestFramework(const QString &testFramework) { _testFramework = testFramework; }

QString TestInfo::suiteName() const { return _suiteName; }

void TestInfo::setSuiteName(const QString &suiteName) { _suiteName = suiteName; }

QString TestInfo::testName() const { return _testName; }

void TestInfo::setTestName(const QString &testName) { _testName = testName; }

bool TestInfo::isValid() const
{
    return !_absoluteFilename.isEmpty() && !_testFramework.isEmpty() && !_suiteName.isEmpty()
           && !_testName.isEmpty();
}

QtUnitTest::Internal::TestInfo::TestInfo(const QString absoluteFilename,
                                         const QString testFramework, const QString suiteName,
                                         const QString testName)
{
    _absoluteFilename = absoluteFilename;
    _testFramework = testFramework;
    _suiteName = suiteName;
    _testName = testName;
}

Message::Message() {}

Message::Message(Message::TestState testState)
{
    _line = -1;
    _text.clear();
    _absoluteFilename.clear();
    _testState = testState;
}

Message::Message(Message::TestState testState, QString text)
{
    _testState = testState;
    _text = text;
    _line = -1;
    _absoluteFilename.clear();
}

Message::Message(TestState testState, QString text, QString absoluteFilename, int line)
{
    _line = line;
    _absoluteFilename = absoluteFilename;
    _text = text;
    _testState = testState;
}

Message::Message(Message::TestState testState, QString absoluteFilename, int line)
{
    _line = line;
    _absoluteFilename = absoluteFilename;
    _testState = testState;
}

Message &Message::operator=(const Message &rhs)
{
    if (this == &rhs)
        return *this;
    _line = rhs._line;
    _text = rhs._text;
    _testState = rhs._testState;
    _absoluteFilename = rhs._absoluteFilename;
    return *this;
}

int Message::line() const { return _line; }

void Message::setLine(int line) { _line = line; }

QString Message::text() const { return _text; }

void Message::setText(const QString &text) { _text = text; }

Message::TestState Message::testState() const { return _testState; }

void Message::setTestState(const TestState &testState) { _testState = testState; }

QString Message::absoluteFilename() const { return _absoluteFilename; }

QString Message::fileName() const { return QFileInfo(_absoluteFilename).fileName(); }

void Message::setAbsoluteFilename(const QString &absoluteFilename)
{
    _absoluteFilename = absoluteFilename;
}

void Message::addTextToMessage(const QString additionalText)
{
    if (_text.isEmpty())
        _text = additionalText;
    else
    {
        _text.append(QLatin1String("\n"));
        _text.append(additionalText);
    }
}

Message::TestState Message::worseCurrentState(const Message::TestState newState,
                                              const Message::TestState currentState)
{
    return newState > currentState ? newState : currentState;
}
QString Message::dataTagText() const { return _dataTagText; }

void Message::setDataTagText(const QString &dataTagText) { _dataTagText = dataTagText; }

TestStorage *TestStorage::_instance = nullptr;

TestStorage::TestStorage(QObject *parent) : QObject(parent) {}

TestStorage *TestStorage::instance()
{
    if (_instance == nullptr)
        _instance = new TestStorage();
    return _instance;
}

ExecutionInfo TestStorage::executionInfoFor(const TestInfo &testInfo) const
{
    return _storage.value(testInfo, ExecutionInfo());
}

void TestStorage::setExecutionInfo(const TestInfo &testInfo, const ExecutionInfo &executionInfo)
{
    ExecutionInfo oldInfo = _storage.value(testInfo);
    _storage[testInfo] = executionInfo;
    if (oldInfo.runStatus() != executionInfo.runStatus())
        emit testUpdated(testInfo, oldInfo);
}

QList< TestInfo > TestStorage::findTestInfo(const QString testFramework, const QString suiteName,
                                            const QString testName) const
{
    QList< TestInfo > foundKeys;
    foreach (const TestInfo &testInfo, _storage.keys())
    {
        if (testInfo.testFramework() == testFramework && testInfo.suiteName() == suiteName
            && testInfo.testName() == testName)
            foundKeys.append(testInfo);
    }
    return foundKeys;
}

QList< TestInfo > TestStorage::findTestInfoWithFiles(const QString testFramework,
                                                     const QString suiteName,
                                                     const QString testName,
                                                     const QStringList sourceFiles) const
{
    QList< TestInfo > foundTestInfo = findTestInfo(testFramework, suiteName, testName);
    QMutableListIterator< TestInfo > testIter(foundTestInfo);
    while (testIter.hasNext())
    {
        TestInfo testInfo = testIter.next();
        if (!sourceFiles.contains(testInfo.absoluteFilename()))
            testIter.remove();
    }
    return foundTestInfo;
}

QString TestStorage::findFileForTest(const QString testFramework, const QString suiteName,
                                     const QString testName) const
{
    QList< TestInfo > tests = findTestInfo(testFramework, suiteName, testName);
    if (tests.isEmpty())
    {
        qWarning() << "failed to get tests for" << suiteName << ":" << testName;
        return QString();
    }
    // FIXME: this file may not contain test implementation (it may be header)
    return tests.at(0).absoluteFilename();
}

QList< TestInfo > TestStorage::allTests() const { return _storage.keys(); }

void TestStorage::insertTests(const QList< TestInfo > testsInfo)
{
    foreach (const TestInfo &info, testsInfo)
        insertTest(info);
}

void TestStorage::removeTests(const QList< TestInfo > testsInfo)
{
    foreach (const TestInfo &info, testsInfo)
        removeTest(info);
}

void TestStorage::insertTest(const TestInfo &testInfo)
{
    if (!_storage.contains(testInfo))
    {
        _storage.insert(testInfo, ExecutionInfo());
        _lastModifiedTest = testInfo;
        emit testAdded(testInfo);
    }
}

void TestStorage::insertTest(const TestInfo &testInfo, const ExecutionInfo &executionInfo)
{
    if (!_storage.contains(testInfo))
    {
        _storage.insert(testInfo, executionInfo);
        _lastModifiedTest = testInfo;
        emit testAdded(testInfo);
    }
}

void TestStorage::updateTest(const TestInfo &testInfo, const ExecutionInfo &executionInfo)
{
    if (!_storage.contains(testInfo))
    {
        insertTest(testInfo, executionInfo);
        return;
    }
    ExecutionInfo oldInfo = _storage.value(testInfo);
    _storage[testInfo] = executionInfo;
    _lastModifiedTest = testInfo;
    emit testUpdated(testInfo, oldInfo);
    qDebug() << "updated test:";
    qDebug() << "  suite:" << testInfo.suiteName() << "test" << testInfo.testName();
}

void TestStorage::removeTest(const TestInfo &testInfo)
{
    int removedTestCount = _storage.remove(testInfo);
    if (removedTestCount > 0)
        emit testRemoved(testInfo);
}

void TestStorage::clear()
{
    qDebug() << "storage was cleared";
    _storage.clear();
    _lastModifiedTest = TestInfo();
    emit storageCleared();
}

void TestStorage::markAllTestsAsNotRun()
{
    QList< TestInfo > keys = _storage.keys();
    foreach (const TestInfo &testInfo, keys)
        _storage[testInfo].markAsNotRun();
    if (!keys.isEmpty())
        _lastModifiedTest = keys.last();
    emit allTestsMarkedAsNotRun();
}

QList< Message > ExecutionInfo::messages() const { return _messages; }

void ExecutionInfo::setMessages(const QList< Message > &messages) { _messages = messages; }

void ExecutionInfo::appendMessage(const Message message)
{
    if (!_messages.contains(message))
    {
        _messages.append(message);
        _lastMessageIndex = _messages.size() - 1;
    }
    setRunStatusIfWorse(message.testState());
}

void ExecutionInfo::addTextToLastMessage(const QString text)
{
    if (_lastMessageIndex >= _messages.size() || _lastMessageIndex < 0)
        return;
    _messages[_lastMessageIndex].addTextToMessage(text);
}

void ExecutionInfo::addFilenameAndLineToLastMesage(const QString filename, const int line)
{
    if (_lastMessageIndex >= _messages.size() || _lastMessageIndex < 0)
        return;
    _messages[_lastMessageIndex].setAbsoluteFilename(filename);
    _messages[_lastMessageIndex].setLine(line);
}

void ExecutionInfo::addDataTextToLastMessage(const QString dataText)
{
    if (_lastMessageIndex >= _messages.size() || _lastMessageIndex < 0)
        return;
    _messages[_lastMessageIndex].setDataTagText(dataText);
}

void ExecutionInfo::setRunStatusIfWorse(const Message::TestState newRunStatus)
{
    if (newRunStatus > _runStatus)
        _runStatus = newRunStatus;
}
double ExecutionInfo::durationInMiliseconds() const { return _durationInMiliseconds; }

void ExecutionInfo::setDurationInMiliseconds(double durationInMiliseconds)
{
    _durationInMiliseconds = durationInMiliseconds;
}

Message::TestState ExecutionInfo::runStatus() const { return _runStatus; }

void ExecutionInfo::setRunStatus(const Message::TestState &runStatus) { _runStatus = runStatus; }

QString ExecutionInfo::runStatusToString(const Message::TestState rs)
{
    switch (rs)
    {
        case QtUnitTest::Internal::Message::TS_UNDEFINED:
            return QStringLiteral("Just added");
        case QtUnitTest::Internal::Message::TS_PASS:
            return QStringLiteral("Passed");
        case QtUnitTest::Internal::Message::TS_FAIL:
            return QStringLiteral("Failed");
        case QtUnitTest::Internal::Message::TS_WARN:
            return QStringLiteral("Warned");
        case QtUnitTest::Internal::Message::TS_NOT_RUN:
            return QStringLiteral("Not run");
        default:
            return QString();
    };
}

void ExecutionInfo::markAsNotRun()
{
    _runStatus = Message::TS_NOT_RUN;
    _messages.clear();
    _lastMessageIndex = -1;
    _durationInMiliseconds = 0.0;
}
