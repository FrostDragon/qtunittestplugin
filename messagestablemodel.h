#ifndef MESSAGESTABLEMODEL_H
#define MESSAGESTABLEMODEL_H

#include <QAbstractTableModel>
#include "teststorage.h"

namespace QtUnitTest
{
namespace Internal
{

class MessagesTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit MessagesTableModel(QObject *parent = 0);
    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    void clearModel();

signals:

public slots:
    void setMessages(const QList< Message > messages);
    QPair< QString, int > fileAndLineForIndex(const QModelIndex &index);

private:
    bool insertRow(int row, Message message, const QModelIndex &parent = QModelIndex());
    bool removeRow(int row, const QModelIndex &parent = QModelIndex());
    QVariant getDisplayRoleValue(const QModelIndex &index) const;
    QVariant getDecorationRoleValue(const QModelIndex &index) const;

    QList< Message > _messages;

    const QString DATA_DRIVEN_PREFIX;
};
}
}

#endif  // MESSAGESTABLEMODEL_H
