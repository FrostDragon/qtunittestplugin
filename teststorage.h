#ifndef TESTSTORAGE_H
#define TESTSTORAGE_H

#include <QObject>

namespace QtUnitTest
{
namespace Internal
{

class TestInfo
{
public:
    TestInfo() {}
    TestInfo(const QString absoluteFilename, const QString testFramework, const QString suiteName,
             const QString testName);

    QString absoluteFilename() const;
    void setAbsoluteFilename(const QString &absoluteFilename);

    QString testFramework() const;
    void setTestFramework(const QString &testFramework);

    QString suiteName() const;
    void setSuiteName(const QString &suiteName);

    QString testName() const;
    void setTestName(const QString &testName);

    bool isValid() const;

private:
    QString _absoluteFilename;
    QString _testFramework;
    QString _suiteName;
    QString _testName;
};

inline uint qHash(const TestInfo &key, uint seed)
{
    return qHash(key.absoluteFilename(), seed) ^ qHash(key.testFramework(), seed)
           ^ qHash(key.suiteName(), seed) ^ qHash(key.testName(), seed);
}

inline bool operator==(const TestInfo &first, const TestInfo &second)
{
    return first.absoluteFilename() == second.absoluteFilename()
           && first.testFramework() == second.testFramework()
           && first.suiteName() == second.suiteName() && first.testName() == second.testName();
}

inline bool operator<(const TestInfo &first, const TestInfo &second)
{
    if (first.absoluteFilename() != second.absoluteFilename())
        return first.absoluteFilename() < second.absoluteFilename();
    if (first.testFramework() != second.testFramework())
        return first.testFramework() < second.testFramework();
    if (first.suiteName() != second.suiteName())
        return first.suiteName() < second.suiteName();
    return first.testName() < second.testName();
}

/*!
 * \brief The Message class stores all information about parsed QtTestLib
 *        message
 */
class Message
{
public:
    enum TestState
    {
        TS_UNDEFINED,
        TS_NOT_RUN,
        TS_PASS,
        TS_WARN,
        TS_FAIL
    };

    Message();
    Message(const TestState testState);
    Message(const TestState testState, const QString text);
    Message(const TestState testState, const QString text, const QString absoluteFilename,
            const int line);
    Message(const TestState testState, const QString absoluteFilename, const int line);
    Message &operator=(const Message &rhs);

    int line() const;
    void setLine(int line);

    QString text() const;
    void setText(const QString &text);

    TestState testState() const;
    void setTestState(const TestState &testState);

    QString absoluteFilename() const;
    QString fileName() const;
    void setAbsoluteFilename(const QString &absoluteFilename);

    void addTextToMessage(const QString additionalText);
    static TestState worseCurrentState(const TestState newState, const TestState currentState);

    QString dataTagText() const;
    void setDataTagText(const QString &dataTagText);

    bool isParametrized() const { return !_dataTagText.isEmpty(); }

private:
    int _line;
    QString _absoluteFilename;
    QString _text;
    TestState _testState;
    QString _dataTagText;
};

inline bool operator==(const Message &m1, const Message &m2)
{
    return m1.line() == m2.line() && m1.text() == m2.text() && m1.testState() == m2.testState()
           && m1.absoluteFilename() == m2.absoluteFilename();
}

class ExecutionInfo
{
public:
    ExecutionInfo() {}

    QList< Message > messages() const;
    void setMessages(const QList< Message > &messages);
    void appendMessage(const Message message);

    void addTextToLastMessage(const QString text);
    void addFilenameAndLineToLastMesage(const QString filename, const int line);
    void addDataTextToLastMessage(const QString dataText);

    Message::TestState runStatus() const;
    void setRunStatus(const Message::TestState &runStatus);
    static QString runStatusToString(const Message::TestState rs);
    void markAsNotRun();

    double durationInMiliseconds() const;
    void setDurationInMiliseconds(double durationInMiliseconds);

private:
    void setRunStatusIfWorse(const Message::TestState newRunStatus);

    QList< Message > _messages;
    int _lastMessageIndex{-1};
    Message::TestState _runStatus{Message::TS_UNDEFINED};
    double _durationInMiliseconds{0.0};
};

// NOTE: not thread-safe
class TestStorage : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(TestStorage)

public:
    explicit TestStorage(QObject *parent = 0);
    static TestStorage *instance();
    ExecutionInfo executionInfoFor(const TestInfo &testInfo) const;
    void setExecutionInfo(const TestInfo &testInfo, const ExecutionInfo &executionInfo);
    TestInfo lastModifiedTest() const { return _lastModifiedTest; }
    QList< TestInfo > findTestInfo(const QString testFramework, const QString suiteName,
                                   const QString testName) const;
    QList< TestInfo > findTestInfoWithFiles(const QString testFramework, const QString suiteName,
                                            const QString testName,
                                            const QStringList sourceFiles) const;
    QString findFileForTest(const QString testFramework, const QString suiteName,
                            const QString testName) const;
    QList< TestInfo > allTests() const;

signals:
    void testAdded(const TestInfo &testInfo);
    void testUpdated(const TestInfo &testInfo, const ExecutionInfo oldInfo);
    void testRemoved(const TestInfo &testInfo);
    void storageCleared();
    void allTestsMarkedAsNotRun();

public slots:
    void insertTest(const TestInfo &testInfo);
    void insertTest(const TestInfo &testInfo, const ExecutionInfo &executionInfo);
    void insertTests(const QList< TestInfo > testsInfo);

    void removeTest(const TestInfo &testInfo);
    void removeTests(const QList< TestInfo > testsInfo);

    void updateTest(const TestInfo &testInfo, const ExecutionInfo &executionInfo);
    void clear();
    void markAllTestsAsNotRun();

private:
    static TestStorage *_instance;
    QMap< TestInfo, ExecutionInfo > _storage;
    TestInfo _lastModifiedTest;
};
}
}

#endif  // TESTSTORAGE_H
