#include "testoutputpane.h"

#include <projectexplorer/projectexplorer.h>
#include <projectexplorer/runconfiguration.h>
#include <utils/outputformat.h>
#include "itemlistwidget.h"
#include "constants.h"
#include <QDebug>

using namespace QtUnitTest::Internal;
using namespace QtUnitTest::Constants;

TestOutputPane::TestOutputPane(QObject *parent) : IOutputPane(parent)
{
    _paneWidget = new PaneWidget();
    _testsFilterCombobox = new QComboBox();
    QStringList items = _paneWidget->playlistManager()->playlistNames();
    _testsFilterCombobox->addItems(items);
    _testsFilterCombobox->setCurrentText(_paneWidget->playlistManager()->activePlaylist().name());
    connect(_paneWidget, &PaneWidget::popupWidget, this, &TestOutputPane::popupHandler);

    _runAllTestsButton = new QToolButton();
    _runAllTestsButton->setIcon(QIcon(QLatin1String(ProjectExplorer::Constants::ICON_RUN_SMALL)));
    _runAllTestsButton->setToolTip(QObject::tr("Run all unit tests for current session"));

    _stopTestsButton = new QToolButton();
    _stopTestsButton->setIcon(QIcon(QLatin1String(ProjectExplorer::Constants::ICON_STOP)));
    _stopTestsButton->setToolTip(QObject::tr("stop tests running for current session"));

    _runSelectedButton = new QToolButton();
    _runSelectedButton->setIcon(QIcon(QLatin1String(ICON_RUN_SELECTED)));
    _runSelectedButton->setToolTip(QObject::tr("run selected tests only"));

    _selectTestsButton = new QToolButton();
    _selectTestsButton->setIcon(QIcon(QLatin1String(ICON_SELECT_TESTS)));
    _selectTestsButton->setToolTip(QObject::tr("select tests to run"));

    _deletePlaylistButton = new QToolButton();
    _deletePlaylistButton->setIcon(QIcon(QLatin1String(ICON_DELETE_PLAYLIST)));
    _deletePlaylistButton->setToolTip(QObject::tr("remove current playlist"));
    updateDeletePlaylistButtonVisibility(_paneWidget->playlistManager()->activePlaylist().name());
    connect(_deletePlaylistButton, SIGNAL(clicked(bool)), this, SLOT(removeCurrentPlaylist()));

    connect(_runAllTestsButton, &QAbstractButton::clicked, _paneWidget, &PaneWidget::runAllTests);
    connect(_stopTestsButton, &QAbstractButton::clicked, _paneWidget, &PaneWidget::stopTests);

    connect(_paneWidget, &PaneWidget::aboutToRunTests, this, &TestOutputPane::runTestsHandler);
    connect(_paneWidget, &PaneWidget::aboutToStopTests, this, &TestOutputPane::stopTestsHandler);

    connect(_paneWidget->playlistManager(), SIGNAL(playlistAdded(QString)), this,
            SLOT(updatePlaylistCombobox()));
    connect(_paneWidget->playlistManager(), SIGNAL(playlistRemoved(QString)), this,
            SLOT(updatePlaylistCombobox()));

    connect(_testsFilterCombobox, SIGNAL(currentTextChanged(QString)),
            paneWidget()->playlistManager(), SLOT(setActivePlaylist(QString)));
    connect(_testsFilterCombobox, SIGNAL(currentTextChanged(QString)), this,
            SLOT(updateDeletePlaylistButtonVisibility(QString)));

    _stopTestsButton->setEnabled(false);
    _playlistLabel = new QLabel(QObject::tr("   Playlist: "));

    // FIXME: this buttons temporarely disabled. Remove them or use for something
    _runSelectedButton->setEnabled(false);
    _runSelectedButton->setVisible(false);
    _selectTestsButton->setEnabled(false);
    _selectTestsButton->setVisible(false);
}

TestOutputPane::~TestOutputPane()
{
    delete _paneWidget;
    delete _testsFilterCombobox;
    delete _runAllTestsButton;
    delete _stopTestsButton;
    delete _runSelectedButton;
    delete _selectTestsButton;
    delete _deletePlaylistButton;
    delete _playlistLabel;
}

QWidget *TestOutputPane::outputWidget(QWidget * /*parent*/) { return _paneWidget; }

QList< QWidget * > TestOutputPane::toolBarWidgets() const
{
    return QList< QWidget * >() << _stopTestsButton << _runAllTestsButton << _runSelectedButton
                                << _selectTestsButton << _playlistLabel << _testsFilterCombobox
                                << _deletePlaylistButton;
}

QString TestOutputPane::displayName() const { return tr("Unit Tests"); }

int TestOutputPane::priorityInStatusBar() const { return 0; }

void TestOutputPane::clearContents() { paneWidget()->clearTableWithErrors(); }

void TestOutputPane::visibilityChanged(bool visible) { _paneWidget->setVisible(visible); }

void TestOutputPane::setFocus() { _paneWidget->setFocus(); }

bool TestOutputPane::hasFocus() const { return _paneWidget->hasFocus(); }

bool TestOutputPane::canFocus() const { return true; }

bool TestOutputPane::canNavigate() const { return false; }

bool TestOutputPane::canNext() const { return false; }

bool TestOutputPane::canPrevious() const { return false; }

void TestOutputPane::goToNext() {}

void TestOutputPane::goToPrev() {}

void TestOutputPane::stopTestsHandler()
{
    _runAllTestsButton->setEnabled(true);
    _deletePlaylistButton->setEnabled(true);
    _stopTestsButton->setEnabled(false);
    emit stopEnabled(false);
}

void TestOutputPane::runTestsHandler()
{
    _runAllTestsButton->setEnabled(false);
    _deletePlaylistButton->setEnabled(false);
    emit runAllEnabled(false);
    _stopTestsButton->setEnabled(true);
    emit stopEnabled(true);
}

void TestOutputPane::updatePlaylistCombobox()
{
    QStringList allPlaylists = _paneWidget->playlistManager()->playlistNames();
    _testsFilterCombobox->clear();
    _testsFilterCombobox->addItems(allPlaylists);
    _testsFilterCombobox->setCurrentText(_paneWidget->playlistManager()->activePlaylist().name());
}

void TestOutputPane::updateDeletePlaylistButtonVisibility(const QString selectedPlaylist)
{
    _deletePlaylistButton->setVisible(selectedPlaylist != Playlist::allTestPlaylistName());
}

void TestOutputPane::removeCurrentPlaylist()
{
    QString currentPlaylist = _testsFilterCombobox->currentText();
    paneWidget()->playlistManager()->removePlaylist(currentPlaylist);
}

PaneWidget *TestOutputPane::paneWidget() const { return _paneWidget; }

void TestOutputPane::setPaneWidget(PaneWidget *paneWidget) { _paneWidget = paneWidget; }

Settings TestOutputPane::settings() const { return _settings; }

void TestOutputPane::setSettings(const Settings &settings)
{
    _settings = settings;
    _paneWidget->setSettings(_settings);
}
