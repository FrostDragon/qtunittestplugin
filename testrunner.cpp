#include "testrunner.h"

#include <projectexplorer/buildmanager.h>
#include <projectexplorer/session.h>
#include <projectexplorer/project.h>
#include <projectexplorer/projectexplorer.h>
#include <projectexplorer/runconfiguration.h>

#include <QThread>


using namespace QtUnitTest::Internal;
using namespace ProjectExplorer;

bool TestRunner::_testsRun = false;

TestRunner::TestRunner(QObject *parent) : QObject(parent)
{
    _parser = new QtTestOutputParser();
    QThread *_parserThread = new QThread(this);
    _parser->moveToThread(_parserThread);
    _testsRun = false;

    _testExecutionStatus = TE_SUCCESS;
    connect(&_runTimer, SIGNAL(timeout()), this, SLOT(runTimerHandler()));
    _runTimer.setSingleShot(true);

    _currentControl = NULL;
    _currentTestFinished = true;
    connect(ProjectExplorerPlugin::instance(),
            SIGNAL(runControlStarted(ProjectExplorer::RunControl *)), this,
            SLOT(setRunControl(ProjectExplorer::RunControl *)));
    //    connect(SessionManager::instance(), SIGNAL(startupProjectChanged(ProjectExplorer::Project
    //    *)),
    //            this, SLOT(handleActiveProjectChanged(ProjectExplorer::Project *)));
}

void QtUnitTest::Internal::TestRunner::runTests(
    const QList< QtUnitTest::Internal::TestInfo > testsToRun)
{
    if (_testsRun)
    {
        qDebug() << "tests are run now";
        return;
    }
    _testExecutionStatus = TE_SUCCESS;
    _testsRun = false;
    _testRunConfigurationIndex = 0;
    emit aboutToRunTests();
    if (!_runConfigurations.isEmpty())
        qDeleteAll(_runConfigurations);
    _runConfigurations.clear();

    TestRunConfigurationGenerator generator;
    _runConfigurations = generator.createConfigurations(testsToRun);
    if (_runConfigurations.isEmpty())
    {
        qDebug() << "run configurations is empty. Can't run tests";
        emit testsFinished();
        return;
    }

    if (BuildManager::isBuilding())
    {
        _testExecutionStatus = TE_RUN_DURING_BUILD;
        emit testsFinished();
        return;
    }

    Target *target = SessionManager::startupProject()->activeTarget();
    if (target == NULL)
    {
        emit testsFinished();
        return;
    }
    _testsRun = true;
    _parser->init(_runConfigurations[0]->workingDirectory(),
                  _runConfigurations[0]->sourceFilesWithTests());
    ProjectExplorerPlugin::instance()->runRunConfiguration(_runConfigurations[0], NormalRunMode,
                                                           true);
    _runTimer.start(50);
}

void TestRunner::stopTests()
{
    if (!_testsRun)
    {
        emit testsFinished();
        _testExecutionStatus = TE_SUCCESS;
        return;
    }
    _testsRun = false;
    if (_currentControl != NULL && _currentControl->isRunning())
    {
        _currentControl->stop();
        stopTestRunControl();
    }

    emit testsFinished();
    _testExecutionStatus = TE_TERMINATE;
}

TestRunner::TestExecutionStatus TestRunner::testExecutionStatus() const
{
    return _testExecutionStatus;
}

void TestRunner::setRunControl(RunControl *newRunControl)
{
    if (_currentControl != NULL)
        disconnect(_currentControl, SIGNAL(finished()), this, SLOT(stopTestRunControl()));
    bool isRunControlExternal = true;
    foreach (TestRunConfiguration *config, _runConfigurations)
    {
        if (config->displayName() == newRunControl->displayName())
        {
            isRunControlExternal = false;
            break;
        }
    }
    if (isRunControlExternal)
        return;

    _currentControl = newRunControl;

    if (_currentControl != NULL && _testRunConfigurationIndex >= 0
        && _testRunConfigurationIndex < _runConfigurations.size()
        && _currentControl->displayName()
               == _runConfigurations[_testRunConfigurationIndex]->displayName())
    {
        _currentTestFinished = false;
        connect(_currentControl, SIGNAL(finished()), this, SLOT(stopTestRunControl()));
        if (_testsRun)
        {
            connect(_currentControl, SIGNAL(appendMessage(ProjectExplorer::RunControl *, QString,
                                                          Utils::OutputFormat)),
                    this, SLOT(appendMessageHandler(ProjectExplorer::RunControl *, QString,
                                                    Utils::OutputFormat)));
        }
    }
}

void TestRunner::stopTestRunControl()
{
    _currentTestFinished = true;
    if (_currentControl != NULL)
    {
        disconnect(_currentControl, SIGNAL(appendMessage(ProjectExplorer::RunControl *, QString,
                                                         Utils::OutputFormat)),
                   this, SLOT(appendMessageHandler(ProjectExplorer::RunControl *, QString,
                                                   Utils::OutputFormat)));
    }
    _currentControl = NULL;
}

void TestRunner::appendMessageHandler(RunControl *runControl, QString msg,
                                         Utils::OutputFormat format)
{
    Q_UNUSED(format);
    if (runControl != NULL)
        _parser->parse(msg);
}

void TestRunner::runTimerHandler()
{
    if (!_currentTestFinished)
        _runTimer.start(50);
    else
    {
        if (_testRunConfigurationIndex >= _runConfigurations.size() - 1
            || _testRunConfigurationIndex < 0)
        {
            finishTestsRunning();
            return;
        }
        else
        {
            if (BuildManager::isBuilding())
            {
                stopTests();
                _testExecutionStatus = TE_RUN_DURING_BUILD;
            }
            _testRunConfigurationIndex++;
            _parser->init(_runConfigurations[_testRunConfigurationIndex]->workingDirectory(),
                          _runConfigurations[_testRunConfigurationIndex]->sourceFilesWithTests());
            ProjectExplorerPlugin::instance()->runRunConfiguration(
                _runConfigurations[_testRunConfigurationIndex], NormalRunMode, true);
            _runTimer.start(50);
        }
    }
}

void TestRunner::finishTestsRunning()
{
    _testsRun = false;
    emit testsFinished();
}
