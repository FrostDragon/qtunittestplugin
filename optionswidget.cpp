#include "optionswidget.h"
#include "ui_optionswidget.h"


using namespace QtUnitTest::Internal;

OptionsWidget::OptionsWidget(QWidget *parent) : QWidget(parent), ui(new Ui::OptionsWidget)
{
    ui->setupUi(this);
}

OptionsWidget::~OptionsWidget() { delete ui; }

Settings OptionsWidget::settings()
{
    _settings = settingsFromUi();
    return _settings;
}

void OptionsWidget::setSettings(const Settings &settings)
{
    _settings = settings;
    settingsToUi(_settings);
}

void
QtUnitTest::Internal::OptionsWidget::settingsToUi(const QtUnitTest::Internal::Settings settings)
{
    QStringList projectsToExclude = settings.projectsToExclude;
    for (int i = 0; i < projectsToExclude.size(); ++i)
        projectsToExclude[i] = projectsToExclude[i].trimmed();
    ui->lineEdit->setText(projectsToExclude.join(QLatin1String(", ")));
}

Settings OptionsWidget::settingsFromUi() const
{
    Settings settings;
    QStringList projectsToExclude
        = ui->lineEdit->text().split(QLatin1String(",", QString::SkipEmptyParts));
    for (int i = 0; i < projectsToExclude.size(); ++i)
        projectsToExclude[i] = projectsToExclude[i].trimmed();
    settings.projectsToExclude = projectsToExclude;
    return settings;
}
