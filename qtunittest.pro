DEFINES += QTUNITTEST_LIBRARY

CONFIG += c++11
# QtUnitTest files

SOURCES += \
    testoutputpane.cpp \
    panewidget.cpp \
    messagestablemodel.cpp \
    qtunittestplugin.cpp \
    itemlistwidget.cpp \
    settings.cpp \
    optionspage.cpp \
    optionswidget.cpp \
    testtreedelegate.cpp \
    testtreemodel.cpp \
    qttestoutputparser.cpp \
    testmonitor.cpp \
    teststorage.cpp \
    testrunconfigurationgenerator.cpp \
    playlistmanager.cpp \
    createplaylistdialog.cpp \
    testrunner.cpp

HEADERS += \
    testoutputpane.h \
    panewidget.h \
    messagestablemodel.h \
    qtunittest_global.h \
    qtunittestplugin.h \
    qtunittestconstants.h \
    itemlistwidget.h \
    constants.h \
    settings.h \
    optionspage.h \
    optionswidget.h \
    testtreedelegate.h \
    testtreemodel.h \
    qttestoutputparser.h \
    testmonitor.h \
    teststorage.h \
    testrunconfigurationgenerator.h \
    playlistmanager.h \
    createplaylistdialog.h \
    testrunner.h

# Qt Creator linking

## set the QTC_SOURCE environment variable to override the setting here
QTCREATOR_SOURCES = $$(QTC_SOURCE)
isEmpty(QTCREATOR_SOURCES):QTCREATOR_SOURCES=D:\SourceCode\QtCreatorPlugins\Qtc\QtCreator_3.4.0_src

## set the QTC_BUILD environment variable to override the setting here
IDE_BUILD_TREE = $$(QTC_BUILD)
isEmpty(IDE_BUILD_TREE):IDE_BUILD_TREE=D:\SourceCode\QtCreatorPlugins\Qtc\QtCreator_3.4.0-bin

## uncomment to build plugin into user config directory
## <localappdata>/plugins/<ideversion>
##    where <localappdata> is e.g.
##    "%LOCALAPPDATA%\QtProject\qtcreator" on Windows Vista and later
##    "$XDG_DATA_HOME/data/QtProject/qtcreator" or "~/.local/share/data/QtProject/qtcreator" on Linux
##    "~/Library/Application Support/QtProject/Qt Creator" on Mac
# USE_USER_DESTDIR = yes

###### If the plugin can be depended upon by other plugins, this code needs to be outsourced to
###### <dirname>_dependencies.pri, where <dirname> is the name of the directory containing the
###### plugin's sources.

QTC_PLUGIN_NAME = QtUnitTest
QTC_LIB_DEPENDS += \
    # nothing here at this time

QTC_PLUGIN_DEPENDS += \
    coreplugin \
    projectexplorer \
    qmakeprojectmanager

QTC_PLUGIN_RECOMMENDS += \
    # optional plugin dependencies. nothing here at this time

###### End _dependencies.pri contents ######


FORMS += \
    panewidget.ui \
    itemlistwidget.ui \
    optionswidget.ui \
    createplaylistdialog.ui

RESOURCES += \
    Icons.qrc

include($$QTCREATOR_SOURCES/src/qtcreatorplugin.pri)
