#include "testrunconfigurationgenerator.h"

#include <projectexplorer/session.h>
#include <qmakeprojectmanager/qmakeproject.h>
#include <QDebug>
#include <projectexplorer/target.h>
#include <projectexplorer/buildconfiguration.h>

#include <projectexplorer/environmentaspect.h>
#include <utils/environment.h>

using namespace QtUnitTest::Internal;

using namespace ProjectExplorer;
using namespace QmakeProjectManager;

TestRunConfiguration::TestRunConfiguration(
    ProjectExplorer::LocalApplicationRunConfiguration *source,
    const QStringList &additionalWorkingDirs)
    : LocalApplicationRunConfiguration(source->target(), source)
{
    _executable = source->executable();
    _runMode = source->runMode();
    _workingDirectory = source->workingDirectory();
    _commandLineArguments = QStringLiteral("-xml");

    EnvironmentAspect *environment = extraAspect< EnvironmentAspect >();
    Utils::Environment env = environment->environment();
    foreach (QString additionalPath, additionalWorkingDirs)
        env.prependOrSetPath(additionalPath);

    // FIXME: this code override user environment settings. Change it
    QString pathKey;
    QString pathValue;
    for (auto iter = env.constBegin(); iter != env.constEnd(); ++iter)
    {
        QString key = env.key(iter);
        if (key.toUpper() == QStringLiteral("PATH"))
        {
            QString value = env.value(iter);
            pathKey = key;
            pathValue = value;
        }
    }

    QList< Utils::EnvironmentItem > exitingUserItems = environment->userEnvironmentChanges();
    int pathItemIndex = -1;
    for (int i = 0; i < exitingUserItems.size(); ++i)
    {
        if (exitingUserItems[i].name.toUpper() == QStringLiteral("PATH"))
            pathItemIndex = i;
    }
    if (pathItemIndex < 0)
        exitingUserItems.append(Utils::EnvironmentItem(pathKey, pathValue));
    else
        exitingUserItems[pathItemIndex] = Utils::EnvironmentItem(pathKey, pathValue);
    environment->setUserEnvironmentChanges(exitingUserItems);
}

QString TestRunConfiguration::executable() const { return _executable; }

QStringList TestRunConfiguration::sourceFilesWithTests() const { return _sourceFilesWithTests; }

void TestRunConfiguration::setSourceFilesWithTests(const QStringList &sourceFilesWithTests)
{
    _sourceFilesWithTests = sourceFilesWithTests;
}

TestRunConfigurationGenerator::TestRunConfigurationGenerator(QObject *parent) : QObject(parent) {}

QStringList TestRunConfigurationGenerator::getExecutablePathesForProFileNodes(
    QSet< QmakeProFileNode * > projects)
{
    QStringList workingDirs;
    foreach (QmakeProFileNode *node, projects)
    {
        QString buildTarget = node->targetInformation().buildTarget;
        if(!buildTarget.isEmpty())
            workingDirs.append(node->buildDir() + QDir::separator() + buildTarget);
    }
    return workingDirs;
}

QList< TestRunConfiguration * >
TestRunConfigurationGenerator::createConfigurations(const QList< TestInfo > tests)
{
    QList< TestRunConfiguration * > configurations;
    if (!startupProjectSupported())
        return configurations;
    QSet< QmakeProFileNode * > projects = avaliableProjects(false);
    QStringList workingDirs = getExecutablePathesForProFileNodes(projects);
    projects = avaliableProjects();
    foreach (QmakeProFileNode *proFileNode, projects)
    {
        QStringList sourceFiles = testSourceFilesForNode(proFileNode, tests);
        if (sourceFiles.isEmpty())
            continue;
        QList< RunConfiguration * > projectRunConfigurations = proFileNode->runConfigurations();
        if (projectRunConfigurations.isEmpty())
        {
            qWarning() << "run configurations for" << proFileNode->displayName() << "is empty";
            continue;
        }
        RunConfiguration *firstRunConfig = projectRunConfigurations.at(0);
        LocalApplicationRunConfiguration *localRunConfig
            = qobject_cast< LocalApplicationRunConfiguration * >(firstRunConfig);
        if (localRunConfig == nullptr)
        {
            qWarning() << "run configurations for" << proFileNode->displayName() << "is not local";
            continue;
        }
        TestRunConfiguration *testRunConfig = new TestRunConfiguration(localRunConfig, workingDirs);
        testRunConfig->setSourceFilesWithTests(sourceFiles);
        configurations.append(testRunConfig);
    }
    return configurations;
}

bool TestRunConfigurationGenerator::startupProjectSupported() const
{
    Project *project = NULL;
    if ((project = SessionManager::startupProject()) == NULL)
        return false;
    if (qobject_cast< QmakeProject * >(project) == NULL)
        return false;
    return true;
}

QSet< QmakeProFileNode * >
TestRunConfigurationGenerator::avaliableProjects(bool excludeNonRunnable) const
{
    if (!startupProjectSupported())
        return QSet< QmakeProFileNode * >();
    Project *project = SessionManager::startupProject();
    QmakeProject *qmakeProject = qobject_cast< QmakeProject * >(project);
    QList< QmakeProFileNode * > allProFiles = qmakeProject->allProFiles();
    if (excludeNonRunnable)
        allProFiles = qmakeProject->nodesWithQtcRunnable(allProFiles);
    return QSet< QmakeProFileNode * >::fromList(allProFiles);
}

QStringList TestRunConfigurationGenerator::testSourceFilesForNode(const QmakeProFileNode *node,
                                                                  const QList< TestInfo > &tests)
{
    if (node == nullptr)
        return QStringList();
    QStringList projectFiles = filesForProject(node);
    QStringList sourceFilesWithUnitTest;
    foreach (const TestInfo &testInfo, tests)
    {
        if (projectFiles.contains(testInfo.absoluteFilename()))
            sourceFilesWithUnitTest.append(testInfo.absoluteFilename());
    }
    return sourceFilesWithUnitTest;
}

QStringList TestRunConfigurationGenerator::filesForProject(const FolderNode *root) const
{
    QStringList list;
    if (root == NULL)
        return list;

    QList< ProjectExplorer::FileNode * > fileNodes = root->fileNodes();
    QList< ProjectExplorer::FolderNode * > subFolderNodes = root->subFolderNodes();
    foreach (const ProjectExplorer::FileNode *file, fileNodes)
    {
        if (file->isGenerated())
            continue;
        if (file->fileType() != HeaderType && file->fileType() != SourceType)
            continue;
        list << file->path().toString();
    }

    foreach (const ProjectExplorer::FolderNode *folder, subFolderNodes)
    {
        if (folder->nodeType() != ProjectExplorer::FolderNodeType
            && folder->nodeType() != ProjectExplorer::VirtualFolderNodeType)
            continue;
        list << filesForProject(folder);
    }
    return list;
}
