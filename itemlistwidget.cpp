#include "itemlistwidget.h"
#include "ui_itemlistwidget.h"

#include "constants.h"

using namespace QtUnitTest::Internal;
using namespace QtUnitTest::Constants;

ItemListWidget::ItemListWidget(QStringList itemsToSelect, QStringList selectedItems,
                               QWidget *parent)
    : QDialog(parent), ui(new Ui::ItemListWidget)
{
    ui->setupUi(this);
    _itemsToSelect = itemsToSelect;
    _itemsToSelect.sort(Qt::CaseInsensitive);

    connect(ui->selectAllButton, SIGNAL(clicked()), this, SLOT(checkAll()));
    connect(ui->deselectAllButton, SIGNAL(clicked()), this, SLOT(uncheckAll()));

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    _model = new CheckableListModel(this);
    _model->setItems(itemsToSelect, selectedItems);

    _proxyModel = new QSortFilterProxyModel(this);
    _proxyModel->setSourceModel(_model);
    ui->listView->setModel(_proxyModel);
    _clearSearchAction
        = ui->searchLineEdit->addAction(QIcon(QLatin1String(ICON_REMOVE)),
                                        QLineEdit::TrailingPosition);
    _clearSearchAction->setEnabled(false);
    ui->searchLineEdit->addAction(QIcon(QLatin1String(ICON_SEARCH)),
                                            QLineEdit::LeadingPosition);
    connect(_clearSearchAction, SIGNAL(triggered()), ui->searchLineEdit, SLOT(clear()));
    connect(ui->searchLineEdit, SIGNAL(textChanged(QString)), this,
            SLOT(changeClearActionVisibility(QString)));
    connect(ui->searchLineEdit, SIGNAL(textEdited(QString)), this,
            SLOT(changeClearActionVisibility(QString)));

    connect(ui->searchLineEdit, SIGNAL(textChanged(QString)), this, SLOT(setFilter(QString)));
    connect(ui->searchLineEdit, SIGNAL(textEdited(QString)), this, SLOT(setFilter(QString)));
}

ItemListWidget::~ItemListWidget() { delete ui; }

QStringList ItemListWidget::selectedItems() const { return _model->selectedItems(); }

void ItemListWidget::checkAll() { _model->checkAll(); }

void ItemListWidget::uncheckAll() { _model->uncheckAll(); }

void ItemListWidget::setFilter(QString text)
{
    _proxyModel->setFilterRegExp(QRegExp(text, Qt::CaseInsensitive, QRegExp::FixedString));
    //    _proxyModel->setFilterKeyColumn(1);
}

void ItemListWidget::changeClearActionVisibility(QString searchText)
{
    if (searchText.isEmpty())
        _clearSearchAction->setEnabled(false);
    else
        _clearSearchAction->setEnabled(true);
}

void CheckableListModel::setItems(QStringList itemsToSelect, QStringList selectedItems)
{
    beginResetModel();
    _itemsToCheck.clear();
    QStringList sortedInputItems = itemsToSelect;
    sortedInputItems.sort(Qt::CaseInsensitive);
    foreach (QString item, sortedInputItems)
    {
        if (selectedItems.contains(item))
            _itemsToCheck.append(qMakePair(item, true));
        else
            _itemsToCheck.append(qMakePair(item, false));
    }
    endResetModel();
}

CheckableListModel::CheckableListModel(QObject *parent) : QAbstractListModel(parent) {}

QStringList CheckableListModel::selectedItems() const
{
    QStringList checkedItems;
    for (int i = 0; i < _itemsToCheck.size(); ++i)
    {
        if (_itemsToCheck[i].second)
            checkedItems.append(_itemsToCheck[i].first);
    }
    return checkedItems;
}

void CheckableListModel::checkAll()
{
    for (int i = 0; i < _itemsToCheck.size(); ++i)
        _itemsToCheck[i].second = true;
    if (!_itemsToCheck.isEmpty())
        emit dataChanged(QAbstractListModel::createIndex(0, 0),
                         QAbstractListModel::createIndex(0, rowCount(QModelIndex()) - 1));
}

void CheckableListModel::uncheckAll()
{
    for (int i = 0; i < _itemsToCheck.size(); ++i)
        _itemsToCheck[i].second = false;
    if (!_itemsToCheck.isEmpty())
        emit dataChanged(QAbstractListModel::createIndex(0, 0),
                         QAbstractListModel::createIndex(0, rowCount(QModelIndex()) - 1));
}

int CheckableListModel::rowCount(const QModelIndex &) const { return _itemsToCheck.size(); }

QVariant CheckableListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= _itemsToCheck.size())
        return QVariant();
    if (role == Qt::DisplayRole || role == Qt::EditRole)
        return _itemsToCheck.at(index.row()).first;
    else if (role == Qt::CheckStateRole)
        return _itemsToCheck.at(index.row()).second ? Qt::Checked : Qt::Unchecked;
    return QVariant();
}

bool CheckableListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.row() < 0 || index.row() >= _itemsToCheck.size())
        return false;
    if (role == Qt::CheckStateRole)
    {
        _itemsToCheck[index.row()].second = static_cast< Qt::CheckState >(value.toUInt())
                                            == Qt::Checked;
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

Qt::ItemFlags CheckableListModel::flags(const QModelIndex &index) const
{
    if (index.row() < 0 || index.row() >= _itemsToCheck.size())
        return Qt::NoItemFlags;
    return Qt::ItemIsUserCheckable | Qt::ItemIsEnabled;
}

SearchLineEdit::SearchLineEdit(QWidget *parent) : QLineEdit(parent)
{
    _clearButton = new QToolButton(this);
    _clearButton->setIcon(QIcon(QLatin1String(ICON_REMOVE)));
    _clearButton->setStyleSheet(QLatin1String("border: 0px; padding: 0px;"));
    _clearButton->setCursor(Qt::ArrowCursor);

    connect(_clearButton, SIGNAL(clicked()), this, SLOT(clear()));
    int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    QSize buttonSize = _clearButton->sizeHint();
    setStyleSheet(QString(QLatin1String("QLineEdit {padding-right: %1px; }"))
                      .arg(buttonSize.width() + frameWidth + 1));
    setMinimumSize(std::max(minimumSizeHint().width(), buttonSize.width() + frameWidth * 2 + 2),
                   std::max(minimumSizeHint().height(), buttonSize.height() + frameWidth * 2 + 2));
    setAttribute(Qt::WA_LayoutUsesWidgetRect);
}

void SearchLineEdit::resizeEvent(QResizeEvent *)
{
    QSize buttonSize = _clearButton->sizeHint();
    int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    _clearButton->move(rect().right() - frameWidth - _clearButton->width(),
                       (rect().bottom() - buttonSize.height() + 1) / 2);
    //    resizeEvent(ev);
}
