#ifndef QTTESTOUTPUTPARSER_H
#define QTTESTOUTPUTPARSER_H

#include <QObject>
#include <QXmlStreamReader>

#include "teststorage.h"

namespace QtUnitTest
{
namespace Internal
{

class QtTestOutputParser : public QObject
{
    Q_OBJECT
public:
    explicit QtTestOutputParser(QObject *parent = 0);
    void init(const QString workingDirectory, const QStringList actualTestSourceFiles);
    bool parse(const QString &message);

    QString workingDirectory() const;
    void setWorkingDirectory(const QString &workingDirectory);

    QStringList actualTestSourceFiles() const;
    void setActualTestSourceFiles(const QStringList &actualTestSourceFiles);

private:
    void processTestSuiteTag();
    void processTestMethodTag();
    bool processIncidentTag();
    void processDurationTag();
    void processDataTag();
    void processDescriptionTag();
    bool processMessageTag();

    void resetTestRelatedVariables();
    bool isPrematureEndOfDocument() const;
    QString toAbsoluteFilename(const QString &relativeFilename) const;

    Message::TestState testState(const QString state) const;
    QString _workingDirectory;
    QXmlStreamReader _xmlStreamReader;

    QString _lastTestSuite;
    QString _lastTestMethod;

    // test-related variables
    QString _lastFile;
    int _lastLine;
    Message::TestState _lastState;
    QString _lastDataTagText;
    QString _lastDescriptionText;
    QStringList _actualTestSourceFiles;

    bool _isXmlReaderReset{true};
};
}
}
#endif  // QTTESTOUTPUTPARSER_H
