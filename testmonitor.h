#ifndef TESTMONITOR_H
#define TESTMONITOR_H

#include <QObject>
#include <QRegularExpression>

#include <cpptools/cppmodelmanager.h>

#include "constants.h"
#include "teststorage.h"

namespace ProjectExplorer
{
class FolderNode;
}

namespace CPlusPlus
{
class Snapshot;
}

namespace QtUnitTest
{
namespace Internal
{
using namespace ProjectExplorer;

class TestMonitor : public QObject
{
    Q_OBJECT
public:
    explicit TestMonitor(QObject *parent = 0);
    virtual QString testFramework() const = 0;
signals:
    void testsAdded(const QList< TestInfo > testInfo);
    void testsRemoved(const QList< TestInfo > testInfo);
    void allProjectTestsRemoved();
};

class QtTestMonitor : public TestMonitor
{
    Q_OBJECT
public:
    QtTestMonitor(QObject *parent = 0);
    virtual ~QtTestMonitor() {}
    QString testFramework() const { return QtUnitTest::Constants::QT_TEST_FRAMEWORK; }

public slots:
    void updateTestsForFileInSnapshot(CPlusPlus::Document::Ptr updatedDocument);
    void removeTestsForFiles(QStringList filesToRemove);

private slots:
    void resetTestSnapshot();
    void removeProjectFromSnapshot(ProjectExplorer::Project *project);

private:
    QList< CPlusPlus::Document::Ptr > findIncludedDocumentsFor(CPlusPlus::Document::Ptr inputDoc,
                                                               const QStringList &projectFiles);

    typedef QMap< QString, QSet< TestInfo > > TestSnapshot;
    QStringList sourceFilesForActiveProject() const;
    QStringList filesForFolder(const ProjectExplorer::FolderNode *root) const;
    QSet< TestInfo > findTestsInDocument(const QStringList suites,
                                         const CPlusPlus::Document::Ptr doc);
    bool isTestProduceXmlOutput(const QString &testName) const;
    QStringList findTestSuites(const QString filename) const;
    inline QString activeProjectName() const;
    // key is absolute file path
    QMap< QString, TestSnapshot > _projectTestSnapshots;
    const QRegularExpression UNIT_TEST_MACRO{QStringLiteral(
                                                 "QTEST_(APPLESS_|GUILESS_)?MAIN\\((\\w+)\\)"),
                                             QRegularExpression::MultilineOption
                                                 | QRegularExpression::DotMatchesEverythingOption};
};
}
}
#endif  // TESTMONITOR_H
