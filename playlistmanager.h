#ifndef PLAYLISTMANAGER_H
#define PLAYLISTMANAGER_H

#include <QObject>
#include <QList>
#include <QStringList>

#include "teststorage.h"

namespace QtUnitTest
{
namespace Internal
{
class Playlist
{
public:
    enum class PlaylistType
    {
        AllTests,
        Custom
    };
    Playlist();
    Playlist(const PlaylistType playlistType, const QString name = QString(),
             const QList< TestInfo > testlist = QList< TestInfo >());
    virtual ~Playlist() {}

    QString name() const;
    void setName(const QString &name);
    QList< TestInfo > testList() const;
    PlaylistType playlistType() const;
    void setPlaylistType(const PlaylistType &playlistType);

    bool addTest(const TestInfo &testInfo);           // return true, if given test does not exist
    int addTests(const QList< TestInfo > &testInfo);  // return added test count

    int removeTest(const TestInfo &testInfo);  // returns number of deleted tests
    void clear();
    bool contains(const TestInfo &testInfo) const;
    static QString allTestPlaylistName() { return ALL_TEST_PLAYLIST_NAME; }

private:
    PlaylistType _playlistType{PlaylistType::AllTests};
    QString _name;
    QList< TestInfo > _tests;
    static const QLatin1String ALL_TEST_PLAYLIST_NAME;
};

class PlaylistManager : public QObject
{
    Q_OBJECT
public:
    explicit PlaylistManager(QObject *parent = 0);
    QStringList playlistNames() const;
    QStringList customPlaylistNames() const;

    bool addPlaylist(const Playlist playlist);
    void removePlaylist(const QString playlistName);
    Playlist activePlaylist() const;

signals:
    void activePlaylistChanged();
    void testAddedToActivePlaylist(const TestInfo &testInfo);
    void testRemovedFromActivePlaylist(const TestInfo &testInfo);
    void playlistAdded(const QString name);
    void playlistRemoved(const QString name);

public slots:
    void addTest(const QString playlistName, const TestInfo &testInfo);
    void addTests(const QString playlistName, const QList<TestInfo> &testInfo);
    void removeTest(const QString playlistName, const TestInfo &testInfo);
    void removeTests(const QString playlistName, const QList<TestInfo> &testInfo);
    void setActivePlaylist(const QString &name);

private:
    QList< Playlist > _allPlaylists;
    int _activePlaylistIndex{0};
};
}
}

#endif  // PLAYLISTMANAGER_H
