#ifndef CREATEPLAYLISTDIALOG_H
#define CREATEPLAYLISTDIALOG_H

#include <QDialog>

namespace Ui
{
class CreatePlaylistDialog;
}

namespace QtUnitTest
{
namespace Internal
{

class CreatePlaylistDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreatePlaylistDialog(QStringList exitingPlaylistNames, QWidget *parent = 0);
    ~CreatePlaylistDialog();
    QString playlistName() const;

private slots:
    void handleNameEditing(const QString &text);

private:
    Ui::CreatePlaylistDialog *ui;
    QStringList _exitingPlaylistNames;
    QString _defaultStyleSheet;
};
}
}
#endif  // CREATEPLAYLISTDIALOG_H
