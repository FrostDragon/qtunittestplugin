#include "qtunittestplugin.h"
#include "qtunittestconstants.h"

#include <coreplugin/icore.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/editormanager/editormanager.h>
#include <projectexplorer/projectexplorer.h>
#include <projectexplorer/buildmanager.h>

#include <QAction>
#include <QMessageBox>
#include <QMainWindow>
#include <QMenu>

#include <QtPlugin>

#include <QDebug>

#include "constants.h"

using namespace QtUnitTest::Internal;
using namespace QtUnitTest::Constants;
using namespace ProjectExplorer;
using namespace Core;

QtUnitTestPlugin::QtUnitTestPlugin()
{
    // Create your members
    _pane = NULL;
    _settings.load(Core::ICore::settings());
}

QtUnitTestPlugin::~QtUnitTestPlugin()
{
    // Unregister objects from the plugin manager's object pool
    // Delete members
    _settings.save(Core::ICore::settings());
}

void QtUnitTestPlugin::createOutputPane()
{
    if (_pane == NULL)
    {
        _pane = new TestOutputPane(this);
        addAutoReleasedObject(_pane);
        _pane->setSettings(_settings);
    }
}

void QtUnitTestPlugin::createMenu()
{
    _runAllAction = new QAction(QObject::tr("Run all"), this);
    _runAllAction->setIcon(QIcon(QLatin1String(ProjectExplorer::Constants::ICON_RUN)));
    _runAllAction->setEnabled(false);
    Core::Command *runAllCmd
        = ActionManager::registerAction(_runAllAction, RUN_ALL_TESTS_ACTION_ID,
                                        Context(Core::Constants::C_GLOBAL));
    runAllCmd->setDefaultKeySequence(QKeySequence(QObject::tr("Alt+T, Alt+A")));

    _stopAction = new QAction(QObject::tr("Stop"), this);
    _stopAction->setIcon(QIcon(QLatin1String(ProjectExplorer::Constants::ICON_STOP)));
    _stopAction->setEnabled(false);
    Core::Command *stopCmd
        = ActionManager::registerAction(_stopAction, STOP_TESTS_ACTION_ID,
                                        Context(Core::Constants::C_GLOBAL));
    stopCmd->setDefaultKeySequence(QKeySequence(QObject::tr("Alt+T, Alt+S")));
    ActionContainer *menu = ActionManager::createMenu(Constants::MENU_ID);
    menu->menu()->setTitle(tr("Unit Tests"));
    menu->addAction(runAllCmd);
    menu->addAction(stopCmd);
    ActionManager::actionContainer(Core::Constants::M_TOOLS)->addMenu(menu);

    connect(_runAllAction, SIGNAL(triggered()), _pane->paneWidget(), SLOT(runAllTests()));
    connect(_stopAction, SIGNAL(triggered()), _pane->paneWidget(), SLOT(stopTests()));

    connect(_pane, SIGNAL(runAllEnabled(bool)), _runAllAction, SLOT(setEnabled(bool)));
    connect(_pane, SIGNAL(stopEnabled(bool)), _stopAction, SLOT(setEnabled(bool)));
}

bool QtUnitTestPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    // Register objects in the plugin manager's object pool
    // Load settings
    // Add actions to menus
    // Connect to other plugins' signals
    // In the initialize function, a plugin can be sure that the plugins it
    // depends on have initialized their members.

    Q_UNUSED(arguments)
    Q_UNUSED(errorString)

    createOutputPane();
    //FIXME: commented options page
    //createOptionsPage();
    createMenu();
    return true;
}

void QtUnitTestPlugin::extensionsInitialized()
{
    // Retrieve objects from the plugin manager's object pool
    // In the extensionsInitialized function, a plugin can be sure that all
    // plugins that depend on it are completely initialized.
}

ExtensionSystem::IPlugin::ShutdownFlag QtUnitTestPlugin::aboutToShutdown()
{
    // Save settings
    // Disconnect from signals that are not needed during shutdown
    // Hide UI (if you add UI that is not in the main window directly)
    return SynchronousShutdown;
}

void QtUnitTestPlugin::createOptionsPage()
{
    _optionsPage = new OptionsPage(_settings);
    addAutoReleasedObject(_optionsPage);
    connect(_optionsPage, SIGNAL(settingsChanged(Settings)), SLOT(onSettingsChanged(Settings)));
}

void QtUnitTestPlugin::onSettingsChanged(const Settings &settings)
{
    _settings = settings;
    _settings.save(Core::ICore::settings());
    _pane->setSettings(_settings);
}

// Q_EXPORT_PLUGIN2(QtUnitTest, QtUnitTestPlugin)
