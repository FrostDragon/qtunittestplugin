#include "testtreedelegate.h"

#include <QPainter>
#include <QIcon>
#include "constants.h"

#include <QApplication>
#include <QDebug>
#include <QStyle>

#include "testtreemodel.h"

using namespace QtUnitTest::Internal;
using namespace QtUnitTest::Constants;

TestTreeDelegate::TestTreeDelegate(QObject *parent) : QStyledItemDelegate(parent) {}

TestTreeDelegate::~TestTreeDelegate() {}

QRect TestTreeDelegate::calculateRectForTestCountInfo(const QRect &optionRect, const QFont font,
                                                      const QString text) const
{
    QFontMetrics fontMetrics(font);
    int helperTextWidth = fontMetrics.width(text);
    QRect helperTextRect = optionRect;
    int helperTextXCoord = optionRect.x() + optionRect.width() - (helperTextWidth + 10);

    helperTextRect.setX(helperTextXCoord);
    if (helperTextWidth < optionRect.width())
        helperTextRect.setWidth(helperTextWidth);
    else
        helperTextRect.setWidth(optionRect.width());
    return helperTextRect;
}

void TestTreeDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                             const QModelIndex &index) const
{
    if (!index.isValid())
    {
        QStyledItemDelegate::paint(painter, option, index);
        return;
    }

    int itemTypeCode = index.model()->data(index, TestTreeModel::TREE_ITEM_TYPE_ROLE).toInt();
    //    if(itemTypeCode == TreeItemNew::RunStatus)
    //    {
    //        QStyledItemDelegate::paint(painter, option, index);
    //        return;
    //    }
    // draw original item without text and icon
    QStyledItemDelegate::paint(painter, option, QModelIndex());
    painter->save();

    // now prepare own icon and text for item
    QIcon icon = index.model()->data(index, Qt::DecorationRole).value< QIcon >();
    QRect iconRect = option.rect;
    iconRect.setTop(option.rect.top() + (option.rect.height() - 16) / 2);

    iconRect.setWidth(16);
    iconRect.setHeight(16);

    QPixmap pixmapToDraw;
    QString itemText = index.model()->data(index, Qt::DisplayRole).toString();
    QString testCountText
        = index.model()->data(index, TestTreeModel::TEST_COUNT_TEXT_ROLE).toString();
    QString durationText;

    durationText = index.model()->data(index, TestTreeModel::TOTAL_DURATION_ROLE).toString();
    if (!durationText.isEmpty())
        durationText.append(QLatin1Char(' '));
    bool iconRequired = itemTypeCode != TreeItem::RunStatus;

    QPair< QRect, QRect > rects
        = rectsForTestCaseAndCount(painter, option, durationText, iconRequired);

    QFontMetrics fontMetrics(painter->font());
    itemText = fontMetrics.elidedText(itemText + QLatin1Char(' ') + testCountText, Qt::ElideRight,
                                      rects.first.width());
    durationText = fontMetrics.elidedText(durationText, Qt::ElideLeft, rects.second.width());

    pixmapToDraw = icon.pixmap(16, 16);

    // draw own text and icon on item
    painter->drawPixmap(iconRect, pixmapToDraw);
    painter->drawText(rects.first, itemText);
    QFont currentFont = painter->font();
    currentFont.setItalic(true);
    painter->setFont(currentFont);
    QPalette palette = QApplication::palette();
    QColor disabledTextColor = palette.color(QPalette::Disabled, QPalette::WindowText);
    painter->setPen(QPen(disabledTextColor));
    painter->drawText(rects.second, durationText);
    painter->restore();
}

QPair< QRect, QRect > TestTreeDelegate::rectsForTestCaseAndCount(const QPainter *painter,
                                                                 const QStyleOptionViewItem &option,
                                                                 const QString testCountText,
                                                                 const bool iconRequired) const
{
    int iconOffset = iconRequired ? -18 : 0;
    QRect actualRect = option.rect + QMargins(iconOffset, 0, -5, 0);
    QFontMetrics fontMetrics(painter->font());

    QRect nodeRect = actualRect;
    QRect testCountRect = actualRect;

    int testCountWidth = fontMetrics.width(testCountText);
    int nodeNameWidth = 0;

    // shrink test name width
    if (actualRect.width() > testCountWidth)
        nodeNameWidth = actualRect.width() - testCountWidth;
    else
    {
        nodeNameWidth = 0;
        testCountWidth = actualRect.width();
    }

    nodeRect.setWidth(nodeNameWidth);
    testCountRect.setX(actualRect.x() + nodeNameWidth);
    testCountRect.setWidth(testCountWidth);
    return qMakePair(nodeRect, testCountRect);
}
