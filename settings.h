#ifndef SETTINGS_H
#define SETTINGS_H

#include <QStringList>

class QSettings;

namespace QtUnitTest
{
namespace Internal
{
class Settings
{
public:
    QStringList projectsToExclude;
    void save(QSettings *settings) const;
    void load(QSettings *settings);
    void setDefaults() { projectsToExclude.clear(); }
    bool equals(const Settings &other) const
    {
        return projectsToExclude == other.projectsToExclude;
    }
};

bool operator==(const Settings &first, const Settings &second);
bool operator!=(const Settings &first, const Settings &second);
}
}

#endif  // SETTINGS_H
