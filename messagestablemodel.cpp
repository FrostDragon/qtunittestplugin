#include "messagestablemodel.h"
#include <QIcon>

#include "constants.h"

using namespace QtUnitTest::Internal;

MessagesTableModel::MessagesTableModel(QObject *parent)
    : QAbstractTableModel(parent), DATA_DRIVEN_PREFIX(QLatin1String("[Parameter: \"%1\"]: "))
{
}

int MessagesTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return _messages.size();
}

int MessagesTableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 3;
}

QVariant MessagesTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= _messages.size())
        return QVariant();
    switch (role)
    {
        case Qt::DisplayRole:
            return getDisplayRoleValue(index);
        case Qt::TextAlignmentRole:
            return index.column() == 0 ? QVariant(Qt::AlignLeft | Qt::AlignVCenter)
                                       : QVariant(Qt::AlignCenter);
        case Qt::DecorationRole:
            return getDecorationRoleValue(index);
        default:
            return QVariant();
    }
}

QVariant MessagesTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
    {
        switch (section)
        {
            case 0:
                return tr("Description");
            case 1:
                return tr("File");
            case 2:
                return tr("Line");
            default:
                return QVariant();
        }
    }
    return QVariant();
}

void MessagesTableModel::clearModel()
{
    while (_messages.size() > 0)
        removeRow(_messages.size() - 1);
}

void MessagesTableModel::setMessages(const QList< Message > messages)
{

    clearModel();
    for (int i = 0; i < messages.size(); ++i)
        insertRow(_messages.size(), messages[i]);
}

QPair< QString, int > MessagesTableModel::fileAndLineForIndex(const QModelIndex &index)
{
    if (!index.isValid() || index.row() >= _messages.size())
        return qMakePair(QLatin1String(""), -1);
    return qMakePair(_messages[index.row()].absoluteFilename(), _messages[index.row()].line());
}

bool MessagesTableModel::insertRow(int row, Message message, const QModelIndex &parent)
{
    if (row < 0 || row > rowCount())
        return false;
    beginInsertRows(parent, row, row);
    _messages.insert(row, message);
    endInsertRows();
    return true;
}

bool MessagesTableModel::removeRow(int row, const QModelIndex &parent)
{
    if (row < 0 || row >= rowCount())
        return false;
    beginRemoveRows(parent, row, row);
    _messages.removeAt(row);
    endRemoveRows();
    return true;
}

QVariant MessagesTableModel::getDisplayRoleValue(const QModelIndex &index) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= _messages.size())
        return QVariant();
    switch (index.column())
    {
        case 0:
        {
            if (_messages.at(index.row()).isParametrized())
                return DATA_DRIVEN_PREFIX.arg(_messages.at(index.row()).dataTagText())
                       + _messages.at(index.row()).text().trimmed();
            else
                return _messages.at(index.row()).text().trimmed();
        }
        case 1:
            return !_messages.at(index.row()).fileName().isEmpty()
                       ? _messages.at(index.row()).fileName().trimmed()
                       : QLatin1String("-");
            break;
        case 2:
            return _messages.at(index.row()).line() > 0 ? QVariant(_messages.at(index.row()).line())
                                                        : QLatin1String("-");
            break;
        default:
            return QVariant();
            break;
    }
}

QVariant MessagesTableModel::getDecorationRoleValue(const QModelIndex &index) const
{
    if (!index.isValid())
        return QVariant();
    if (index.column() != 0)
        return QVariant();
    if (index.row() >= _messages.size())
        return QVariant();
    Message::TestState state = _messages.at(index.row()).testState();
    if (state == Message::TS_PASS)
        return QVariant();
    QString pathToIcon;
    switch (state)
    {
        case Message::TS_WARN:
            pathToIcon = QLatin1String(QtUnitTest::Constants::ICON_WARNING);
            break;
        case Message::TS_FAIL:
            pathToIcon = QLatin1String(QtUnitTest::Constants::ICON_FAIL);
            break;
        default:
            pathToIcon = QLatin1String(QtUnitTest::Constants::ICON_QUESTION);
            break;
    }
    return QIcon(pathToIcon);
}
