#include "playlistmanager.h"

using namespace QtUnitTest::Internal;

const QLatin1String Playlist::ALL_TEST_PLAYLIST_NAME = QLatin1String("All tests");

Playlist::Playlist(const PlaylistType playlistType, const QString name,
                   const QList< TestInfo > testlist)
{

    _playlistType = playlistType;
    if (playlistType != PlaylistType::AllTests)
        _name = name;
    _tests = testlist;
}

QString Playlist::name() const
{
    if (_playlistType == PlaylistType::AllTests)
        return ALL_TEST_PLAYLIST_NAME;
    else
        return _name;
}

void Playlist::setName(const QString &name)
{
    if (_playlistType != PlaylistType::AllTests)
        _name = name;
}

QList< TestInfo > Playlist::testList() const
{
    // TODO: remove TestStorage usage here
    if (_playlistType == PlaylistType::AllTests)
        return TestStorage::instance()->allTests();
    else
        return _tests;
}

bool QtUnitTest::Internal::Playlist::addTest(const QtUnitTest::Internal::TestInfo &testInfo)
{
    if (_playlistType != PlaylistType::AllTests && !_tests.contains(testInfo))
    {
        _tests.append(testInfo);
        return true;
    }
    return false;
}

int Playlist::addTests(const QList< TestInfo > &testInfo)
{
    int addedTestCount = 0;
    foreach (const TestInfo &test, testInfo)
    {
        bool isAdded = addTest(test);
        if (isAdded)
            addedTestCount++;
    }
    return addedTestCount;
}

int Playlist::removeTest(const TestInfo &testInfo)
{
    if (_playlistType == PlaylistType::AllTests)
        return 0;
    else
        return _tests.removeAll(testInfo);
}

void Playlist::clear() { _tests.clear(); }

bool Playlist::contains(const TestInfo &testInfo) const
{
    if (_playlistType == PlaylistType::AllTests)
        return testInfo.isValid();
    else
        return _tests.contains(testInfo);
}

Playlist::PlaylistType Playlist::playlistType() const { return _playlistType; }

void Playlist::setPlaylistType(const PlaylistType &playlistType) { _playlistType = playlistType; }

PlaylistManager::PlaylistManager(QObject *parent) : QObject(parent)
{
    _allPlaylists.append(Playlist(Playlist::PlaylistType::AllTests));
    _activePlaylistIndex = 0;
}

QStringList PlaylistManager::playlistNames() const
{
    QStringList names;
    foreach (const Playlist &playlist, _allPlaylists)
        names.append(playlist.name());
    return names;
}

QStringList PlaylistManager::customPlaylistNames() const
{
    QStringList names;
    foreach (const Playlist &playlist, _allPlaylists)
        if (playlist.playlistType() != Playlist::PlaylistType::AllTests)
            names.append(playlist.name());
    return names;
}

bool PlaylistManager::addPlaylist(const Playlist playlist)
{
    if (playlist.playlistType() == Playlist::PlaylistType::AllTests)
        return false;
    if (playlistNames().contains(playlist.name()))
        return false;
    _allPlaylists.append(playlist);
    emit playlistAdded(_allPlaylists.last().name());
    return true;
}

void PlaylistManager::removePlaylist(const QString playlistName)
{
    for (int playlistIndex = 0; playlistIndex < _allPlaylists.size(); ++playlistIndex)
    {
        if (_allPlaylists[playlistIndex].playlistType() == Playlist::PlaylistType::AllTests)
            continue;
        if (_allPlaylists[playlistIndex].name() == playlistName)
        {
            _allPlaylists.removeAt(playlistIndex);
            emit playlistRemoved(playlistName);
            if (playlistIndex == _activePlaylistIndex)
            {
                if (_activePlaylistIndex > 0)
                    _activePlaylistIndex--;
                emit activePlaylistChanged();
            }
            return;
        }
    }
}

Playlist PlaylistManager::activePlaylist() const { return _allPlaylists[_activePlaylistIndex]; }

void PlaylistManager::setActivePlaylist(const QString &name)
{
    for (int playlistIndex = 0; playlistIndex < _allPlaylists.size(); ++playlistIndex)
    {
        if (_allPlaylists[playlistIndex].name() == name)
        {
            if (_activePlaylistIndex != playlistIndex)
            {
                _activePlaylistIndex = playlistIndex;
                emit activePlaylistChanged();
            }
            return;
        }
    }
}

void PlaylistManager::addTest(const QString playlistName, const TestInfo &testInfo)
{
    for (int playlistIndex = 0; playlistIndex < _allPlaylists.size(); ++playlistIndex)
    {
        if (_allPlaylists[playlistIndex].name() != playlistName)
            continue;
        bool isAdded = _allPlaylists[playlistIndex].addTest(testInfo);
        if (isAdded && playlistIndex == _activePlaylistIndex)
            emit testAddedToActivePlaylist(testInfo);
    }
}

void PlaylistManager::addTests(const QString playlistName, const QList<TestInfo> &testInfo)
{
    foreach (const TestInfo &test, testInfo)
        addTest(playlistName, test);
}

void PlaylistManager::removeTest(const QString playlistName, const TestInfo &testInfo)
{
    for (int playlistIndex = 0; playlistIndex < _allPlaylists.size(); ++playlistIndex)
    {
        if (_allPlaylists[playlistIndex].name() != playlistName)
            continue;
        int removedTestCount = _allPlaylists[playlistIndex].removeTest(testInfo);
        if (removedTestCount > 0 && playlistIndex == _activePlaylistIndex)
            emit testRemovedFromActivePlaylist(testInfo);
    }
}

void PlaylistManager::removeTests(const QString playlistName, const QList<TestInfo> &testInfo)
{
    foreach (const TestInfo &test, testInfo)
        removeTest(playlistName, test);
}
