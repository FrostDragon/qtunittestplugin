#ifndef TESTTREEDELEGATE_H
#define TESTTREEDELEGATE_H

#include <QObject>
#include <QStyledItemDelegate>
#include <QMovie>
#include <QTimer>
namespace QtUnitTest
{
namespace Internal
{
class TestTreeDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    TestTreeDelegate(QObject *parent = 0);
    ~TestTreeDelegate();

    // QAbstractItemDelegate interface
public:
    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const;
    QRect calculateRectForTestCountInfo(const QRect &optionRect, const QFont font,
                                        const QString text) const;

signals:
    void repaintRequested();

private:
    // first nodeName rect, second - test count
    QPair< QRect, QRect > rectsForTestCaseAndCount(const QPainter *painter,
                                                   const QStyleOptionViewItem &option,
                                                   const QString testCountText,
                                                   const bool iconRequired) const;
};
}
}
#endif  // TESTTREEDELEGATE_H
