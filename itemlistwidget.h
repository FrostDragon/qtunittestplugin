#ifndef ITEMLISTWIDGET_H
#define ITEMLISTWIDGET_H

#include <QDialog>
#include <QAbstractListModel>
#include <QSortFilterProxyModel>
#include <QLineEdit>
#include <QToolButton>

namespace Ui
{
class ItemListWidget;
}

namespace QtUnitTest
{
namespace Internal
{

class SearchLineEdit : public QLineEdit
{
public:
    SearchLineEdit(QWidget *parent = 0);
    ~SearchLineEdit() {}

private:
    QToolButton *_clearButton;

    // QWidget interface
protected:
    void resizeEvent(QResizeEvent *);
};

class CheckableListModel : public QAbstractListModel
{
public:
    CheckableListModel(QObject *parent = 0);
    ~CheckableListModel() {}
    QStringList selectedItems() const;
    void checkAll();
    void uncheckAll();
    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;
    void setItems(QStringList itemsToSelect, QStringList selectedItems);

private:
    QList< QPair< QString, bool > > _itemsToCheck;
};

class ItemListWidget : public QDialog
{
    Q_OBJECT

public:
    explicit ItemListWidget(QStringList itemsToSelect, QStringList selectedItems = QStringList(),
                            QWidget *parent = 0);
    ~ItemListWidget();
    QStringList selectedItems() const;
private slots:
    void checkAll();
    void uncheckAll();
    void setFilter(QString text);
    void changeClearActionVisibility(QString searchText);
private:
    Ui::ItemListWidget *ui;
    QStringList _itemsToSelect;
    CheckableListModel *_model;
    QSortFilterProxyModel *_proxyModel;
    QAction *_clearSearchAction;
};
}
}

#endif  // ITEMLISTWIDGET_H
