#ifndef CONSTANTS_H
#define CONSTANTS_H

namespace QtUnitTest
{
namespace Constants
{
//icons
const char ICON_WARNING[]         = ":/icons/Resources/warningIcon.png";
const char ICON_FAIL[]            = ":/icons/Resources/errorIcon.png";
const char ICON_QUESTION[]        = ":/icons/Resources/questionIcon.png";
const char ICON_PASS[]            = ":/icons/Resources/accept_16.png";
const char ICON_SEARCH[]          = ":/icons/Resources/searchIcon.png";
const char ICON_REMOVE[]          = ":/icons/Resources/removeIcon.png";
const char ICON_RUN_SELECTED[]    = ":/icons/Resources/run_selected.png";
const char ICON_SELECT_TESTS[]    = ":/icons/Resources/selectTests.png";
const char ICON_REFRESH[]         = ":/icons/Resources/refreshIcon.png";
const char ICON_EXCLUDE_PROJECT[] = ":/icons/Resources/excludeProject.png";
const char ICON_OPTIONS[]         = ":/icons/Resources/UnitTest.png";
const char ICON_PROGRESS[]        = ":/icons/Resources/progress.GIF";
const char ICON_PLUS[]            = ":/icons/Resources/plus.png";
const char ICON_MINUS[]           = ":/icons/Resources/minus.png";
const char ICON_DELETE_PLAYLIST[] = ":/icons/Resources/delete_playlist.png";

//settings
const char SETTINGS_GROUP[] = "QtUnitTest";
const char PROJECTS_TO_EXCLUDE[] = "ProjectsToExclude";

//menu
const char RUN_ALL_TESTS_ACTION_ID[]      = "QtUnitTest.RunTests";
const char STOP_TESTS_ACTION_ID[] = "QtUnitTest.StopTests";

const QString QT_TEST_FRAMEWORK{QLatin1Literal("Qt Test")};

}
}
#endif  // CONSTANTS_H
