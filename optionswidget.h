#ifndef OPTIONSWIDGET_H
#define OPTIONSWIDGET_H

#include <QWidget>
#include "settings.h"

namespace Ui
{
class OptionsWidget;
}

namespace QtUnitTest
{
namespace Internal
{
class OptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OptionsWidget(QWidget *parent = 0);
    ~OptionsWidget();

    Settings settings();
    void setSettings(const Settings &settings);

private:
    void settingsToUi(const Settings settings);
    Settings settingsFromUi() const;
    Settings _settings;
    Ui::OptionsWidget *ui;
};
}
}

#endif  // OPTIONSWIDGET_H
