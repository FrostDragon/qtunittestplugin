#ifndef TESTRUNNER_H
#define TESTRUNNER_H

#include <QObject>
#include <QTimer>

#include "teststorage.h"
#include "testrunconfigurationgenerator.h"
#include "qttestoutputparser.h"

namespace QtUnitTest
{
namespace Internal
{

class TestRunner : public QObject
{
    Q_OBJECT
public:
    enum TestExecutionStatus
    {
        TE_SUCCESS,
        TE_ERROR_OCCURED,
        TE_TERMINATE,
        TE_NOT_A_QMAKE_PROJECT,
        TE_RUN_DURING_BUILD
    };
    explicit TestRunner(QObject *parent = 0);
    TestExecutionStatus testExecutionStatus() const;

signals:
    void aboutToRunTests();
    void testsFinished();

public slots:
    void runTests(const QList< TestInfo > testsToRun);
    void stopTests();

private slots:
    void runTimerHandler();
    void setRunControl(ProjectExplorer::RunControl *newRunControl);
    void stopTestRunControl();
    void appendMessageHandler(ProjectExplorer::RunControl *runControl, QString msg,
                              Utils::OutputFormat format);

private:
    void finishTestsRunning();
    QList< TestRunConfiguration * > _runConfigurations;
    int _testRunConfigurationIndex{-1};

    QtTestOutputParser *_parser;
    static bool _testsRun;
    TestExecutionStatus _testExecutionStatus;
    QTimer _runTimer;
    bool _currentTestFinished;
    ProjectExplorer::RunControl *_currentControl;
};
}
}

#endif  // TESTRUNNER_H
