#include "createplaylistdialog.h"
#include "ui_createplaylistdialog.h"

#include <QPushButton>
#include "constants.h"

using namespace QtUnitTest::Internal;
using namespace QtUnitTest::Constants;

CreatePlaylistDialog::CreatePlaylistDialog(QStringList exitingPlaylistNames, QWidget *parent)
    : QDialog(parent), ui(new Ui::CreatePlaylistDialog)
{
    ui->setupUi(this);
    _exitingPlaylistNames = exitingPlaylistNames;
    _defaultStyleSheet = ui->lineEdit->styleSheet();
    connect(ui->lineEdit, SIGNAL(textChanged(QString)), this, SLOT(handleNameEditing(QString)));
    handleNameEditing(ui->lineEdit->text());
}

CreatePlaylistDialog::~CreatePlaylistDialog() { delete ui; }

void CreatePlaylistDialog::handleNameEditing(const QString &text)
{

    if (text.isEmpty() || _exitingPlaylistNames.contains(text))
    {
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        ui->lineEdit->setStyleSheet(QStringLiteral("QLineEdit { color : red; }"));
        QAction *warningAction = new QAction(ui->lineEdit);
        warningAction->setIcon(QIcon(QLatin1String(ICON_WARNING)));
        if (text.isEmpty())
            warningAction->setToolTip(QObject::tr("name can't be empty"));
        else
            warningAction->setToolTip(QObject::tr("playlist with given name already exist"));
        ui->lineEdit->addAction(warningAction, QLineEdit::TrailingPosition);
    }
    else
    {
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
        ui->lineEdit->setStyleSheet(_defaultStyleSheet);
        QList< QAction * > actions = ui->lineEdit->actions();
        if (actions.size() > 1)
            qDebug() << "some additional actions has been removed";
        foreach (QAction *action, actions)
        {
            ui->lineEdit->removeAction(action);
        }
    }
}

QString CreatePlaylistDialog::playlistName() const { return ui->lineEdit->text(); }
