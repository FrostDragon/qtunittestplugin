#ifndef QTUNITTEST_H
#define QTUNITTEST_H

#include "qtunittest_global.h"
#include <extensionsystem/iplugin.h>
#include "testoutputpane.h"
#include "optionspage.h"

namespace QtUnitTest {
namespace Internal {

class QtUnitTestPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtCreatorPlugin" FILE "QtUnitTest.json")

public:
    QtUnitTestPlugin();
    ~QtUnitTestPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

    void createOptionsPage();
    void createOutputPane();
    void createMenu();

public slots:
    void onSettingsChanged(const Settings &settings);

private:
    TestOutputPane *_pane;
    OptionsPage *_optionsPage;
    Settings _settings;

    QAction *_runAllAction;
    QAction *_stopAction;
};

} // namespace Internal
} // namespace QtUnitTest

#endif // QTUNITTEST_H

