#ifndef QTUNITTESTCONSTANTS_H
#define QTUNITTESTCONSTANTS_H

namespace QtUnitTest {
namespace Constants {

const char ACTION_ID[] = "QtUnitTest.Action";
const char MENU_ID[] = "QtUnitTest.Menu";

} // namespace QtUnitTest
} // namespace Constants

#endif // QTUNITTESTCONSTANTS_H

