#include "testmonitor.h"

#include <cpptools/cppmodelmanager.h>
#include <projectexplorer/projectexplorer.h>
#include <projectexplorer/session.h>
#include <qmakeprojectmanager/qmakeproject.h>

#include <cplusplus/ASTPath.h>

using namespace QtUnitTest::Internal;

using namespace ProjectExplorer;
using namespace CppTools;

TestMonitor::TestMonitor(QObject *parent) : QObject(parent)
{
    TestStorage *storage = TestStorage::instance();
    connect(this, SIGNAL(testsAdded(QList< TestInfo >)), storage,
            SLOT(insertTests(QList< TestInfo >)));
    connect(this, SIGNAL(testsRemoved(QList< TestInfo >)), storage,
            SLOT(removeTests(QList< TestInfo >)));
    connect(this, SIGNAL(allProjectTestsRemoved()), storage, SLOT(clear()));
}

QtTestMonitor::QtTestMonitor(QObject *parent) : TestMonitor(parent)
{
    // new files and updated files
    connect(CppTools::CppModelManager::instance(),
            SIGNAL(documentUpdated(CPlusPlus::Document::Ptr)), this,
            SLOT(updateTestsForFileInSnapshot(CPlusPlus::Document::Ptr)));

    // removed files
    connect(CppTools::CppModelManager::instance(), SIGNAL(aboutToRemoveFiles(QStringList)), this,
            SLOT(removeTestsForFiles(QStringList)));

    connect(SessionManager::instance(), SIGNAL(startupProjectChanged(ProjectExplorer::Project *)),
            this, SLOT(resetTestSnapshot()));
    connect(SessionManager::instance(), SIGNAL(aboutToRemoveProject(ProjectExplorer::Project *)),
            this, SLOT(removeProjectFromSnapshot(ProjectExplorer::Project *)));
}

void QtTestMonitor::removeTestsForFiles(QStringList filesToRemove)
{
    // delete tests for removed files from snapshot
    QStringList removedFiles;
    QString projectName = activeProjectName();
    if (projectName.isEmpty())
    {
        qDebug() << "empty active project name";
        return;
    }
    QStringList filesFromSnapshot = _projectTestSnapshots.value(projectName).keys();

    foreach (QString file, filesToRemove)
    {
        if (filesFromSnapshot.contains(file))
            removedFiles.append(file);
    }

    QSet< TestInfo > removedTests;
    foreach (QString removedFile, removedFiles)
    {
        QSet< TestInfo > testInfoForFile
            = _projectTestSnapshots.value(projectName).value(removedFile);
        removedTests.unite(testInfoForFile);
        _projectTestSnapshots[projectName].remove(removedFile);
    }
    if (!removedTests.isEmpty())
        emit testsRemoved(removedTests.toList());
}

void QtTestMonitor::resetTestSnapshot()
{
    emit allProjectTestsRemoved();
    QString name = activeProjectName();
    if (name.isEmpty())
        return;
    QSet< TestInfo > allTests;
    QMapIterator< QString, QSet< TestInfo > > iter(_projectTestSnapshots[name]);
    while (iter.hasNext())
    {
        iter.next();
        allTests.unite(iter.value());
    }
    if (!allTests.isEmpty())
        emit testsAdded(allTests.toList());
}

void QtTestMonitor::removeProjectFromSnapshot(Project *project)
{
    if (project == nullptr)
        return;
    _projectTestSnapshots.remove(project->displayName());
    if (_projectTestSnapshots.isEmpty())  // ProjectTree is empty
        emit allProjectTestsRemoved();
}

QList< CPlusPlus::Document::Ptr >
QtTestMonitor::findIncludedDocumentsFor(CPlusPlus::Document::Ptr inputDoc,
                                        const QStringList &projectFiles)
{
    QList< CPlusPlus::Document::Ptr > documents;
    if (inputDoc == nullptr)
        return documents;
    CPlusPlus::Snapshot snapshot = CppModelManager::instance()->snapshot();
    foreach (const CPlusPlus::Document::Include &includeFile, inputDoc->resolvedIncludes())
    {
        CPlusPlus::Document::Ptr includedDoc = snapshot.document(includeFile.resolvedFileName());
        if (includedDoc != nullptr && projectFiles.contains(includeFile.resolvedFileName()))
            documents.append(includedDoc);
    }
    return documents;
}

// process every updated file in project
void QtTestMonitor::updateTestsForFileInSnapshot(CPlusPlus::Document::Ptr updatedDocument)
{
    if (updatedDocument == nullptr)
        return;
    QString projectName = activeProjectName();
    if (projectName.isEmpty())
        return;

    QStringList projectFiles = sourceFilesForActiveProject();
    if (!projectFiles.contains(updatedDocument->fileName()))
        return;

    QStringList testSuites = findTestSuites(updatedDocument->fileName());
    if (testSuites.isEmpty())
    {
        removeTestsForFiles(QStringList(updatedDocument->fileName()));
        return;
    }

    QList< CPlusPlus::Document::Ptr > documentsWithPossibleTests
        = findIncludedDocumentsFor(updatedDocument, projectFiles);
    QSet< TestInfo > newTestSet;
    documentsWithPossibleTests.prepend(updatedDocument);
    foreach (CPlusPlus::Document::Ptr document, documentsWithPossibleTests)
    {
        QSet< TestInfo > testsSet = findTestsInDocument(testSuites, document);
        newTestSet.unite(testsSet);
    }

    QString filename = updatedDocument->fileName();
    if (newTestSet.isEmpty())
    {
        // remove exiting tests for doc file
        if (_projectTestSnapshots.value(projectName).contains(filename))
        {
            QSet< TestInfo > testsToRemove
                = _projectTestSnapshots.value(projectName).value(filename);
            if (!testsToRemove.isEmpty())
                emit testsRemoved(testsToRemove.toList());
        }
        _projectTestSnapshots[projectName].remove(filename);
    }
    else
    {
        QSet< TestInfo > testsFromSnapshot
            = _projectTestSnapshots.value(projectName).value(filename);
        QSet< TestInfo > diff = newTestSet;
        diff.subtract(testsFromSnapshot);
        // here is new tests in diff
        if (!diff.isEmpty())
            emit testsAdded(diff.toList());
        diff = testsFromSnapshot;
        diff.subtract(newTestSet);
        // here is removed tests in diff
        if (!diff.isEmpty())
            emit testsRemoved(diff.toList());
        _projectTestSnapshots[projectName][filename] = newTestSet;
    }
}

QStringList QtTestMonitor::sourceFilesForActiveProject() const
{
    QStringList sourceFiles;
    Project *project = SessionManager::startupProject();
    if (project == nullptr)
        return sourceFiles;
    // TODO: replace with generic project support
    QmakeProjectManager::QmakeProject *qmakeProject
        = qobject_cast< QmakeProjectManager::QmakeProject * >(project);
    if (qmakeProject == nullptr)
        return sourceFiles;

    QList< QmakeProjectManager::QmakeProFileNode * > allRunnable
        = qmakeProject->nodesWithQtcRunnable(qmakeProject->allProFiles());
    foreach (QmakeProjectManager::QmakeProFileNode *proFileNode, allRunnable)
    {
        // FIXME: commented settings usage
        //        if
        //        (_settings.projectsToExclude.contains(proFileNode->displayName()))
        //            continue;
        QStringList projectFiles = filesForFolder(proFileNode);
        foreach (QString file, projectFiles)
        {
            if (!sourceFiles.contains(file))
                sourceFiles.append(file);
        }
    }
    return sourceFiles;
}

QStringList QtTestMonitor::filesForFolder(const FolderNode *root) const
{
    QStringList list;
    if (root == NULL)
        return list;

    QList< ProjectExplorer::FileNode * > fileNodes = root->fileNodes();
    QList< ProjectExplorer::FolderNode * > subFolderNodes = root->subFolderNodes();
    foreach (const ProjectExplorer::FileNode *file, fileNodes)
    {
        if (file->isGenerated())
            continue;
        if (file->fileType() != HeaderType && file->fileType() != SourceType)
            continue;
        list << file->path().toString();
    }
    foreach (const ProjectExplorer::FolderNode *folder, subFolderNodes)
    {
        if (folder->nodeType() != ProjectExplorer::FolderNodeType
            && folder->nodeType() != ProjectExplorer::VirtualFolderNodeType)
            continue;
        list << filesForFolder(folder);
    }
    return list;
}

QSet< TestInfo > QtTestMonitor::findTestsInDocument(const QStringList suites,
                                                    const CPlusPlus::Document::Ptr doc)
{
    QSet< TestInfo > tests;
    if (suites.isEmpty() || doc == nullptr)
        return tests;

    for (unsigned index = 0; index < doc->globalSymbolCount(); ++index)
    {
        CPlusPlus::Symbol *symbol = doc->globalSymbolAt(index);
        if (symbol == nullptr)
            continue;
        CPlusPlus::Class *klass = symbol->asClass();
        if (klass == nullptr)
            continue;
        QString klassName
            = QString::fromUtf8(klass->identifier()->chars(), klass->identifier()->size());
        if (!suites.contains(klassName))
            continue;
        for (unsigned i = 0; i < klass->memberCount(); ++i)
        {
            CPlusPlus::Symbol *member = klass->memberAt(i);  // visibility can be get from
            CPlusPlus::Function *fun = member->type()->asFunctionType();  // slot can be got here
            if (member->visibility() != CPlusPlus::Symbol::Private)
                continue;
            if (fun == nullptr || fun->isGenerated() || !fun->isSlot())
                continue;
            QString slotName
                = QString::fromUtf8(fun->identifier()->chars(), fun->identifier()->size());
            if (isTestProduceXmlOutput(slotName))
            {
                TestInfo testInfo(doc->fileName(), testFramework(), klassName, slotName);
                tests.insert(testInfo);
            }
        }
    }

    return tests;
}

bool QtTestMonitor::isTestProduceXmlOutput(const QString &testName) const
{
    return !testName.isEmpty()
           && !testName.endsWith(QStringLiteral("_data")) && testName != QStringLiteral("init")
                                 && testName != QStringLiteral("cleanup");
}

QStringList QtTestMonitor::findTestSuites(const QString filename) const
{
    QStringList foundTestcases;
    QFile inputFile(filename);
    if (!inputFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return foundTestcases;
    QString data = QString::fromLocal8Bit(inputFile.readAll());
    QRegularExpressionMatchIterator matchIterator = UNIT_TEST_MACRO.globalMatch(data);
    while (matchIterator.hasNext())
    {
        QRegularExpressionMatch match = matchIterator.next();
        QString testCaseName = match.captured(2).trimmed();
        if (!testCaseName.isEmpty() && !foundTestcases.contains(testCaseName))
            foundTestcases.append(testCaseName);
    }
    return foundTestcases;
}

QString QtTestMonitor::activeProjectName() const
{
    return SessionManager::startupProject() != nullptr
               ? SessionManager::startupProject()->displayName()
               : QString();
}
