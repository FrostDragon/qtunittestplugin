#ifndef TESTOUTPUTPANE_H
#define TESTOUTPUTPANE_H

#include <coreplugin/ioutputpane.h>
#include <utils/outputformat.h>

#include <QObject>
#include <QComboBox>
#include <QToolButton>
#include <QAction>
#include <QLabel>

#include "panewidget.h"
#include "settings.h"

namespace ProjectExplorer
{
class RunConfiguration;
class RunControl;
}

namespace QtUnitTest {
namespace Internal {

class TestOutputPane : public Core::IOutputPane
{
    Q_OBJECT

public:
    explicit TestOutputPane(QObject *parent = 0);
    ~TestOutputPane();

    QWidget *outputWidget(QWidget *);
    QList< QWidget * > toolBarWidgets() const;
    QString displayName() const;
    int priorityInStatusBar() const;
    void clearContents();
    void visibilityChanged(bool visible);
    void setFocus();
    bool hasFocus() const;
    bool canFocus() const;
    bool canNavigate() const;
    bool canNext() const;
    bool canPrevious() const;
    void goToNext();
    void goToPrev();

    Settings settings() const;
    void setSettings(const Settings &settings);

    PaneWidget *paneWidget() const;
    void setPaneWidget(PaneWidget *paneWidget);

signals:
    void runAllEnabled(bool enabled);
    void runSelectedEnabled(bool enabled);
    void stopEnabled(bool enabled);

private slots:
    void popupHandler() { emit(showPage(WithFocus)); }
    void stopTestsHandler();
    void runTestsHandler();
    void updatePlaylistCombobox();
    void updateDeletePlaylistButtonVisibility(const QString selectedPlaylist);
    void removeCurrentPlaylist();

private:
    PaneWidget *_paneWidget;
    QComboBox *_testsFilterCombobox;
    QToolButton *_runAllTestsButton;
    QToolButton *_runSelectedButton;
    QToolButton *_stopTestsButton;
    QToolButton *_selectTestsButton;
    QToolButton *_deletePlaylistButton;
    QLabel *_playlistLabel;
    Settings _settings;
};
}
}
#endif  // TESTOUTPUTPANE_H
