#include "panewidget.h"
#include "ui_panewidget.h"

#include <coreplugin/editormanager/editormanager.h>
#include <projectexplorer/projectexplorer.h>
#include <QDir>
#include <QMenu>
#include <QAction>

#include "constants.h"
#include "teststorage.h"
#include "testrunconfigurationgenerator.h"
#include "createplaylistdialog.h"

using namespace QtUnitTest::Internal;
using namespace QtUnitTest::Constants;
using namespace ProjectExplorer;

PaneWidget::PaneWidget(QWidget *parent)
    : QWidget(parent),
      ui(new Ui::PaneWidget),
      PROGRESS_TEXT(QLatin1String("run %1 of %2")),
      TESTING_DONE_TEXT(QObject::tr("Testing done with <b>%1 failed, %2 warned, %3 passed</b> and "
                                    "<b>%4 not run</b> testsuites")),
      TESTS_FINISHED_WITH_ERROR_TEXT(QObject::tr("some tests not run or not properly finished")),
      TESTS_TERMINATED_TEXT(QObject::tr("Unit test running was terminated. Information about some "
                                        "tests may be lost")),
      NOT_A_QMAKE_PROJECT_TEXT(QObject::tr("Only qmake-based projects supported")),
      RUN_DURING_BUILD_TEXT(QObject::tr("project build has been run during test running. Tests "
                                        "running terminated"))
{
    ui->setupUi(this);
    _monitor = new QtTestMonitor(this);
    _testRunner = new TestRunner(this);
    _playlistManager = new PlaylistManager(this);

    _messageTableModel = new MessagesTableModel();
    _delegate = new TestTreeDelegate();
    ui->tableView->setModel(_messageTableModel);

    _testTreeModel = new TestTreeModel(_playlistManager, this);
    //    _testTreeFilterProxy = new TestStateFilterModel(this);
    //    _testTreeFilterProxy->setSourceModel(_newModel);
    //    _testTreeFilterProxy->setSortRole(Qt::DisplayRole);
    //    ui->testExplorerView->setModel(_testTreeFilterProxy);

    ui->testExplorerView->setModel(_testTreeModel);
    ui->testExplorerView->setItemDelegate(_delegate);
    ui->testExplorerView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->testExplorerView, SIGNAL(clicked(QModelIndex)), _testTreeModel,
            SLOT(onItemClick(QModelIndex)));
    connect(ui->testExplorerView, SIGNAL(customContextMenuRequested(QPoint)), this,
            SLOT(showContextMenu(QPoint)));
    connect(ui->tableView, &QAbstractItemView::doubleClicked, this, &PaneWidget::openFileInEditor);

    connect(_testTreeModel, SIGNAL(testMessagesRequested(QList< Message >)), this,
            SLOT(setMessagesToMessageTable(QList<Message>)));

    connect(_delegate, SIGNAL(repaintRequested()), this, SLOT(repaintViewPort()));
    connect(_testRunner, &TestRunner::aboutToRunTests, this, &PaneWidget::prepareToTestRun);
    connect(_testRunner, &TestRunner::testsFinished, this,
            &PaneWidget::updatedInfoAboutFinishedTests);

    connect(_testRunner, &TestRunner::aboutToRunTests, this, &PaneWidget::showProgressWidgets);
    connect(_testRunner, &TestRunner::testsFinished, this, &PaneWidget::popup);

    ui->splitter->setStretchFactor(0, 1);
    ui->splitter->setStretchFactor(1, 2);

    ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->setSortingEnabled(true);

    ui->progressBar->setFixedHeight(ui->progressBar->sizeHint().height());
    ui->progressBar->hide();

    ui->errorLabel->setFixedHeight(ui->errorLabel->sizeHint().height());
    ui->errorLabel->hide();
}

PaneWidget::~PaneWidget()
{
    ui->splitter->saveState();
    if (_messageTableModel != NULL)
        delete _messageTableModel;
    delete _delegate;
    delete ui;
}

void PaneWidget::setActivePlaylist(const QString activePlaylistName)
{
    _playlistManager->setActivePlaylist(activePlaylistName);
}

void PaneWidget::setErrorLabelText(const QString &text)
{
    ui->errorLabel->setText(text);
    ui->errorLabel->show();
    ui->errorLabel->setFixedHeight(ui->errorLabel->sizeHint().height());
}

void PaneWidget::updatedInfoAboutFinishedTests()
{
    emit aboutToStopTests();
    ui->progressBar->hide();
    _testTreeModel->setTestRunInProgress(false);
    _testTreeModel->updateDuration();
    _testTreeModel->updateHeader();

    TestRunner::TestExecutionStatus status = _testRunner->testExecutionStatus();
    switch (status)
    {
        case TestRunner::TE_SUCCESS:
            ui->errorLabel->hide();
            ui->errorLabel->setText(QLatin1String(""));
            break;
        case TestRunner::TE_ERROR_OCCURED:
            setErrorLabelText(TESTS_FINISHED_WITH_ERROR_TEXT);
            break;
        case TestRunner::TE_TERMINATE:
            setErrorLabelText(TESTS_TERMINATED_TEXT);
            break;
        case TestRunner::TE_NOT_A_QMAKE_PROJECT:
            setErrorLabelText(NOT_A_QMAKE_PROJECT_TEXT);
            break;
        case TestRunner::TE_RUN_DURING_BUILD:
            setErrorLabelText(RUN_DURING_BUILD_TEXT);
            break;
        default:
            break;
    }
}

void PaneWidget::clearTableWithErrors() { _messageTableModel->clearModel(); }

void PaneWidget::prepareToTestRun()
{
    _testTreeModel->setTestRunInProgress(true);
    _testTreeModel->clearDurationSummary();
    clearTableWithErrors();
    ui->progressBar->setMaximum(0);
}

void PaneWidget::showProgressWidgets()
{
    ui->progressBar->setMaximum(0);
    ui->progressBar->setValue(0);
    ui->progressBar->show();
    // ui->progressLabel->show();
}

void PaneWidget::runAllTests()
{
    emit aboutToRunTests();
    TestStorage::instance()->markAllTestsAsNotRun();
    // I want update model before test run. So I update it here instead of signal/slot connection
    _testTreeModel->syncModelWithActivePlaylist();
    QSet< TestInfo > allTests = QSet< TestInfo >::fromList(TestStorage::instance()->allTests());
    if (_playlistManager->activePlaylist().playlistType() == Playlist::PlaylistType::Custom)
    {
        QSet< TestInfo > playlistTests
            = QSet< TestInfo >::fromList(_playlistManager->activePlaylist().testList());
        allTests.intersect(playlistTests);
    }
    _testRunner->runTests(allTests.toList());
    ui->errorLabel->hide();
    emit popupWidget();
}

void PaneWidget::stopTests()
{
    emit aboutToStopTests();
    _testRunner->stopTests();
    ui->progressBar->setMaximum(1);
    ui->progressBar->setValue(ui->progressBar->maximum());
    ui->errorLabel->show();
    ui->errorLabel->setFixedHeight(ui->errorLabel->sizeHint().height());
    ui->errorLabel->setText(TESTS_TERMINATED_TEXT);
}

void PaneWidget::openFileInEditor(const QModelIndex &index)
{
    QPair< QString, int > fileAndLine = _messageTableModel->fileAndLineForIndex(index);
    if (!fileAndLine.first.isEmpty() && fileAndLine.second >= 0)
        Core::EditorManager::openEditorAt(QFileInfo(fileAndLine.first).absoluteFilePath(),
                                          fileAndLine.second);
    else if (!fileAndLine.first.isEmpty())
        Core::EditorManager::openEditor(QFileInfo(fileAndLine.first).absoluteFilePath());
}

void PaneWidget::mapProxyIndexToSource(const QModelIndex &index)
{
    _testTreeModel->onItemClick(_testTreeFilterProxy->mapToSource(index));
}

void PaneWidget::repaintViewPort() { ui->testExplorerView->viewport()->repaint(); }

void PaneWidget::sortTree() { _testTreeFilterProxy->sort(0); }

void PaneWidget::showContextMenu(const QPoint &pos)
{
    QModelIndex menuIndex = ui->testExplorerView->indexAt(pos);
    TreeItem *item = static_cast< TreeItem * >(menuIndex.internalPointer());
    if (item == nullptr)
        return;
    _contextMenuModelIndex = menuIndex;

    QMenu *playlistMenu = new QMenu(ui->testExplorerView);
    QStringList playlists = _playlistManager->customPlaylistNames();
    playlists.removeAll(_playlistManager->activePlaylist().name());
    foreach (QString playlistName, playlists)
    {
        QAction *exitingPlaylistAction = new QAction(playlistName, ui->testExplorerView);
        playlistMenu->addAction(exitingPlaylistAction);
        connect(exitingPlaylistAction, SIGNAL(triggered(bool)), this,
                SLOT(addTestsToExitingPlaylist()));
    }
    playlistMenu->addSeparator();
    QAction *newPlaylistAction
        = new QAction(QStringLiteral("create new playlist..."), ui->testExplorerView);
    playlistMenu->addAction(newPlaylistAction);
    playlistMenu->setIcon(QIcon(QLatin1String(ICON_PLUS)));

    connect(newPlaylistAction, SIGNAL(triggered()), this, SLOT(showNewPlaylistDialog()));
    QMenu *menu = new QMenu(ui->testExplorerView);

    if (item->type() == TreeItem::Test || item->type() == TreeItem::ParametrizedTest)
    {
        playlistMenu->setTitle(
            QString(QStringLiteral("add \"%1\" to playlist")).arg(item->data().toString()));
        menu->addMenu(playlistMenu);
        if (_playlistManager->activePlaylist().playlistType() != Playlist::PlaylistType::AllTests)
        {
            QAction *removeAction
                = new QAction(QIcon(QLatin1String(ICON_MINUS)),
                              QString(QStringLiteral("remove \"%1\" from current playlist"))
                                  .arg(item->data().toString()),
                              ui->testExplorerView);
            menu->addAction(removeAction);
            connect(removeAction, SIGNAL(triggered(bool)), this,
                    SLOT(removeTestFromCurrentPlaylist()));
        }
    }
    else
    {
        playlistMenu->setTitle(QString(QStringLiteral("add tests from \"%1\" to playlist"))
                                   .arg(item->data().toString()));
        menu->addMenu(playlistMenu);
        if (_playlistManager->activePlaylist().playlistType() != Playlist::PlaylistType::AllTests)
        {
            QAction *removeAction
                = new QAction(QIcon(QLatin1String(ICON_MINUS)),
                              QString(QStringLiteral("remove \"%1\" tests from current playlist"))
                                  .arg(item->data().toString()),
                              ui->testExplorerView);
            menu->addAction(removeAction);
            connect(removeAction, SIGNAL(triggered(bool)), this,
                    SLOT(removeTestFromCurrentPlaylist()));
        }
    }

    menu->popup(ui->testExplorerView->viewport()->mapToGlobal(pos));
}

void PaneWidget::showNewPlaylistDialog()
{
    if (!_contextMenuModelIndex.isValid())
        return;
    TreeItem *menuItem = static_cast< TreeItem * >(_contextMenuModelIndex.internalPointer());
    if (menuItem == nullptr)
        return;

    QStringList playlistNames = _playlistManager->playlistNames();
    CreatePlaylistDialog dialog(playlistNames, this);
    dialog.setWindowTitle(QObject::tr("Create new playlist"));
    int result = dialog.exec();
    if (result == QDialog::Accepted)
    {
        Playlist newPlaylist(Playlist::PlaylistType::Custom, dialog.playlistName());
        QList< TestInfo > tests = _testTreeModel->testListForItem(menuItem);
        newPlaylist.addTests(tests);
        _playlistManager->addPlaylist(newPlaylist);
    }
}

void PaneWidget::addTestsToExitingPlaylist()
{
    QAction *clickedAction = qobject_cast< QAction * >(QObject::sender());
    if (clickedAction == nullptr)
        return;
    TreeItem *menuItem = static_cast< TreeItem * >(_contextMenuModelIndex.internalPointer());
    if (menuItem == nullptr)
        return;
    QString playlistName = clickedAction->text();
    QList< TestInfo > tests = _testTreeModel->testListForItem(menuItem);
    playlistManager()->addTests(playlistName, tests);
}

void PaneWidget::removeTestFromCurrentPlaylist()
{
    TreeItem *menuItem = static_cast< TreeItem * >(_contextMenuModelIndex.internalPointer());
    if (menuItem == nullptr)
        return;
    QList< TestInfo > tests = _testTreeModel->testListForItem(menuItem);
    QString currentPlaylistName = playlistManager()->activePlaylist().name();
    playlistManager()->removeTests(currentPlaylistName, tests);
}

void PaneWidget::setMessagesToMessageTable(const QList<Message> messages)
{
    _messageTableModel->setMessages(messages);
    ui->tableView->setVisible(false);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->setVisible(true);
}

Settings PaneWidget::settings() const { return _settings; }

void PaneWidget::setSettings(const Settings &settings) { _settings = settings; }
