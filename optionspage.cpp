#include "optionspage.h"

#include "constants.h"

using namespace QtUnitTest::Internal;

OptionsPage::OptionsPage(const Settings settings, QObject *parent) : Core::IOptionsPage(parent)
{
    _settings = settings;
    setId("QtUnitTestSettings");
    setDisplayName(tr("Unit Tests"));
    setCategory("Unit Tests");
    setDisplayCategory(tr("Unit Tests"));
    setCategoryIcon(QLatin1String(QtUnitTest::Constants::ICON_OPTIONS));
}

OptionsPage::~OptionsPage() {}

QWidget *OptionsPage::widget()
{
    if(_optionsWidget == NULL)
    {
        _optionsWidget = new OptionsWidget();
        _optionsWidget->setSettings(_settings);
    }
    return _optionsWidget;
}

void OptionsPage::apply()
{
    Settings newSettings = _optionsWidget->settings();
    if(newSettings != _settings)
    {
        _settings = newSettings;
        emit settingsChanged(_settings);
    }
}

void OptionsPage::finish()
{
    delete _optionsWidget;
    _optionsWidget = NULL;
}
