#ifndef PANEWIDGET_H
#define PANEWIDGET_H

#include <QWidget>

#include "messagestablemodel.h"
#include "settings.h"

#include "testtreedelegate.h"

#include "testtreemodel.h"
#include "testmonitor.h"
#include "testrunner.h"
#include "playlistmanager.h"

namespace Ui
{
class PaneWidget;
}

namespace ProjectExplorer
{
class SessionManager;
class Project;
class ProjectNode;
class FolderNode;
class FileNode;
class ProjectTree;
class RunConfiguration;
class Target;
}

namespace QtUnitTest
{
namespace Internal
{

class PaneWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PaneWidget(QWidget *parent = 0);
    ~PaneWidget();

    Settings settings() const;
    void setSettings(const Settings &settings);

    void setErrorLabelText(const QString &text);
    PlaylistManager *playlistManager() const { return _playlistManager; }

public slots:
    void setActivePlaylist(const QString activePlaylistName);
    void updatedInfoAboutFinishedTests();
    void prepareToTestRun();
    void showProgressWidgets();
    void popup() { emit popupWidget(); }
    void runAllTests();
    void stopTests();
    void clearTableWithErrors();

signals:
    void popupWidget();
    void aboutToRunTests();
    void aboutToStopTests();

private slots:
    void openFileInEditor(const QModelIndex &index);
    void mapProxyIndexToSource(const QModelIndex &index);
    void repaintViewPort();
    void sortTree();
    void showContextMenu(const QPoint &pos);
    void showNewPlaylistDialog();
    void addTestsToExitingPlaylist();
    void removeTestFromCurrentPlaylist();
    void setMessagesToMessageTable(const QList<Message> messages);
private:
    // TestsRunner *_runner;

    TestStateFilterModel *_testTreeFilterProxy;

    Ui::PaneWidget *ui;
    MessagesTableModel *_messageTableModel;
    TestTreeDelegate *_delegate;
    Settings _settings;

    const QString PROGRESS_TEXT;
    const QString TESTING_DONE_TEXT;

    const QString TESTS_FINISHED_WITH_ERROR_TEXT;
    const QString TESTS_TERMINATED_TEXT;
    const QString NOT_A_QMAKE_PROJECT_TEXT;
    const QString RUN_DURING_BUILD_TEXT;

    TestTreeModel *_testTreeModel;
    TestRunner *_testRunner;
    TestMonitor *_monitor;
    PlaylistManager *_playlistManager;
    QModelIndex _contextMenuModelIndex;
};
}
}
#endif  // PANEWIDGET_H
