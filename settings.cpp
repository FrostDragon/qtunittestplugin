#include "settings.h"

#include <QSettings>
#include "constants.h"

using namespace QtUnitTest::Constants;

void QtUnitTest::Internal::Settings::save(QSettings *settings) const
{
    settings->beginGroup(QLatin1String(SETTINGS_GROUP));
    settings->beginWriteArray(QLatin1String(PROJECTS_TO_EXCLUDE));
    for (int index = 0; index < projectsToExclude.size(); ++index)
    {
        settings->setArrayIndex(index);
        settings->setValue(QLatin1String("ProjectName"), projectsToExclude[index]);
    }
    settings->endArray();
    settings->endGroup();
}

void QtUnitTest::Internal::Settings::load(QSettings *settings)
{
    setDefaults();
    settings->beginGroup(QLatin1String(SETTINGS_GROUP));
    const int arraySize = settings->beginReadArray(QLatin1String(PROJECTS_TO_EXCLUDE));
    for (int index = 0; index < arraySize; ++index)
    {
        settings->setArrayIndex(index);
        QString projectName = settings->value(QLatin1String("ProjectName")).toString();
        if (!projectName.isEmpty())
            projectsToExclude.append(projectName);
    }
    settings->endArray();
    settings->endGroup();
}

bool QtUnitTest::Internal::operator==(const QtUnitTest::Internal::Settings &first,
                                      const QtUnitTest::Internal::Settings &second)
{
    return first.equals(second);
}

bool QtUnitTest::Internal::operator!=(const QtUnitTest::Internal::Settings &first,
                                      const QtUnitTest::Internal::Settings &second)
{
    return !first.equals(second);
}
