#ifndef TESTTREEMODEL_H
#define TESTTREEMODEL_H

#include <QObject>
#include <QAbstractItemModel>
#include <QSortFilterProxyModel>

#include "teststorage.h"
#include "playlistmanager.h"

namespace QtUnitTest
{
namespace Internal
{

class TreeItem
{
public:
    enum ItemType
    {
        Root,
        RunStatus,
        TestFramework,
        TestSuite,
        Test,
        ParametrizedTest
    };
    TreeItem(const ItemType type, TreeItem *parent = 0);
    TreeItem(const QString text, const ItemType type, TreeItem *parent = 0);
    TreeItem(const TestInfo testInfo, const ItemType type, TreeItem *parent = 0);
    ~TreeItem() { qDeleteAll(_childItems); }

    void appendChild(TreeItem *child) { _childItems.append(child); }
    void removeChild(TreeItem *child);
    void removeChildren();

    TreeItem *child(int row);
    int childCount() const { return _childItems.size(); }
    QVariant data() const;
    int row() const;
    TreeItem *parent() { return _parentItem; }

    TreeItem *findChild(QString name);
    QList< TreeItem * > children() const { return _childItems; }
    bool hasChildren() const { return !_childItems.isEmpty(); }

    ItemType type() const;

    Message::TestState testState() const;
    void setTestState(const Message::TestState &testState);

    bool inProgress() const;
    void setInProgress(bool inProgress);

    QString testCountText() const;
    int testItemCount() const;

    // TODO: it is ugly solution. Create base class and childs for TreeItem or smth else
    TestInfo testInfo() const;
    void setTestInfo(const TestInfo &testInfo);

    double totalDuration() const;
    void setTotalDuration(double totalDuration);

private:
    QList< TreeItem * > _childItems;
    QString _text;
    TreeItem *_parentItem;
    ItemType _type;
    Message::TestState _testState;
    bool _inProgress;
    TestInfo _testInfo;
    double _totalDuration{0.0};
};

class TestTreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    TestTreeModel(PlaylistManager *playlistManager, QObject *parent = NULL);
    static const int TEST_COUNT_TEXT_ROLE = Qt::UserRole + 1;
    static const int TREE_ITEM_TYPE_ROLE = Qt::UserRole + 2;
    static const int TOTAL_DURATION_ROLE = Qt::UserRole + 3;

    TreeItem *createRootItem();

    // QAbstractItemModel interface
public:
    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &) const { return 1; }
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    QList< TestInfo > testListForItem(TreeItem *treeItem) const;
    bool getTestRunInProgress() const;
    void setTestRunInProgress(bool testRunInProgress);

signals:
    void testMessagesRequested(QList< Message >);

public slots:
    void onItemClick(QModelIndex index);
    void clearModel();
    void insertTest(const TestInfo &testInfo);
    void removeTest(const TestInfo &testInfo);
    void updateTest(const TestInfo &testInfo, const ExecutionInfo &oldInfo);
    void syncModelWithActivePlaylist();
    void updateHeader();
    void clearDurationSummary();
    void updateDuration();

private slots:
    void addTestFromStorage(const TestInfo &testInfo);

private:
    void removeTest(const TestInfo &testInfo, const ExecutionInfo &execInfo);

    void setProgressStateFor(TreeItem *root, bool state);
    QList< Message > allWarningsAndFails(TreeItem *rootItem);

    QList< TreeItem * > allItemsWithType(TreeItem *root, const TreeItem::ItemType type) const;

    TreeItem *findChildNodeByNameAndType(TreeItem *baseNode, TreeItem::ItemType nodeType,
                                         const QString nodeName) const;
    TreeItem *findOrCreateRunStatusNode(TreeItem *baseNode, const QString nodeName,
                                        TreeItem *parentForNew);
    TreeItem *findOrCreateTestFrameworkNode(TreeItem *baseNode, const QString testFrameworkName,
                                            TreeItem *parentForNew);
    TreeItem *findOrCreateSuiteNode(TreeItem *frameworkNode, const QString suiteName,
                                    TreeItem *parentForNew);
    TreeItem *findUnitTestNode(TreeItem *testSuiteNode, const QString testName);

    void createUnitTestNode(TreeItem *testSuiteNode, const TestInfo &testInfo,
                            const ExecutionInfo executionInfo);

    QString getPathToIcon(const Message::TestState state) const;
    void populateRunStatusToParents(TreeItem *node, const Message::TestState state);
    void populateAdditinalDuration(TreeItem *node, const double durationDiff);

    QString prettyDurationString(const double durationInMiliseconds) const;
    void setDurationToChilds(TreeItem *rootItem, double duration);

    TreeItem *_rootItem{NULL};
    PlaylistManager *_playlistManager{nullptr};  // TODO: not removed here, maybe it is bad
                                                 // solution?
    QString _headerText;
    bool _testRunInProgress{false};
};

class TestStateFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    enum FilterBehavior
    {
        ShowAll,
        ShowWarningsAndFails,
        ShowFailsOnly
    };

    TestStateFilterModel(QObject *parent = 0);
    virtual ~TestStateFilterModel() {}
    FilterBehavior filterBehavior() const;
public slots:
    void setFilterBehavior(const FilterBehavior &filterBehavior);
    void setFilterBehavior(const int filterBehaviorCode);

    // QSortFilterProxyModel interface
protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const;

private:
    bool canAcceptRow(const TestInfo &testInfo) const;
    FilterBehavior _filterBehavior;
};
}
}
#endif  // TESTTREEMODEL_H
