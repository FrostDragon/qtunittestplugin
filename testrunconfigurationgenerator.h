#ifndef TESTRUNCONFIGURATIONGENERATOR_H
#define TESTRUNCONFIGURATIONGENERATOR_H

#include <QObject>
#include <projectexplorer/localapplicationrunconfiguration.h>

#include "teststorage.h"
#include <projectexplorer/projectnodes.h>

#include <QDebug>

namespace QmakeProjectManager
{
class QmakeProFileNode;
}

namespace ProjectExplorer
{
class FolderNode;
}

namespace QtUnitTest
{
namespace Internal
{
class TestRunConfiguration : public ProjectExplorer::LocalApplicationRunConfiguration
{
    Q_OBJECT
public:
    TestRunConfiguration(ProjectExplorer::LocalApplicationRunConfiguration *source,
                         const QStringList &additionalWorkingDirs);
    virtual ~TestRunConfiguration() {}

    // RunConfiguration interface
public:
    QWidget *createConfigurationWidget() { return nullptr; }

    // LocalApplicationRunConfiguration interface
public:
    QString executable() const;
    ProjectExplorer::ApplicationLauncher::Mode runMode() const { return _runMode; }
    QString workingDirectory() const { return _workingDirectory; }
    QString commandLineArguments() const { return _commandLineArguments; }
    void setArguments(const QString args) { _commandLineArguments = args; }

    QStringList sourceFilesWithTests() const;
    void setSourceFilesWithTests(const QStringList &sourceFilesWithTests);

private:
    QString _executable;
    ProjectExplorer::ApplicationLauncher::Mode _runMode;
    QString _workingDirectory;
    QString _commandLineArguments;
    QStringList _sourceFilesWithTests;
};

class TestRunConfigurationGenerator : public QObject
{
    Q_OBJECT
public:
    explicit TestRunConfigurationGenerator(QObject *parent = 0);
    QList< TestRunConfiguration * > createConfigurations(const QList< TestInfo > tests);

    QStringList
    getExecutablePathesForProFileNodes(QSet< QmakeProjectManager::QmakeProFileNode * > projects);

private:
    bool startupProjectSupported() const;
    QSet< QmakeProjectManager::QmakeProFileNode * > avaliableProjects(bool excludeNonRunnable
                                                                      = true) const;
    QStringList testSourceFilesForNode(const QmakeProjectManager::QmakeProFileNode *node,
                                       const QList< TestInfo > &tests);
    QStringList filesForProject(const ProjectExplorer::FolderNode *root) const;
};
}
}

#endif  // TESTRUNCONFIGURATIONGENERATOR_H
